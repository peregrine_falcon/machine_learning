% Implement a Three Layered Neural Network


% Clear and Close all the windows and variables in Octave
clear; close all; clc;


% Load data
load('MNIST_Digit_Dataset.mat'); % training data stored in arrays X, y

m = size(X, 1);


% Randomly select 100 data points to display
rand_indices = randperm(m);
sel = X(rand_indices(1:100), :);


% Display data
displayData(sel);

% Necessary functions
function g = sigmoid(z)
  g = 1 ./ (1 + e .^ (-z));
endfunction

function W = randInitializeWeights(L_in, L_out)
  W = zeros(L_out, 1 + L_in);
  epsilon_init = 0.12;
  W = rand(L_out, 1 + L_in) * 2 * epsilon_init - epsilon_init;
endfunction

function g = sigmoidGradient(z)
  g = zeros(size(z));
  gz = zeros(size(z));
  gz = sigmoid(z);
  g = gz.*(1 - gz);
endfunction

function p = predict(Theta1, Theta2, X)
  m = size(X, 1);
  num_labels = size(Theta2, 1);

  p = zeros(size(X, 1), 1);

  h1 = sigmoid([ones(m, 1) X] * Theta1');
  h2 = sigmoid([ones(m, 1) h1] * Theta2');
  [dummy, p] = max(h2, [], 2);

endfunction

% Training the Neural Network
fprintf('\nTraining Neural Network... \n')

% Setting parameters
lambda = 1;
input_layer_size  = 400;         % 20x20 Input Images of Digits
hidden_layer_size = 25;          % 25 hidden units
num_labels = 10;                 % 10 labels, from 1 to 10

fprintf('\nInitializing Neural Network Parameters ...\n')
initial_Theta1 = randInitializeWeights(input_layer_size, hidden_layer_size);
initial_Theta2 = randInitializeWeights(hidden_layer_size, num_labels);

% Unroll parameters
initial_nn_params = [initial_Theta1(:) ; initial_Theta2(:)];

options = optimset('MaxIter', 50);
nncostFunction = @(p) costFunction(p, ...
                                   input_layer_size, ...
                                   hidden_layer_size, ...
                                   num_labels, X, y, lambda);

[nn_params, cost] = fmincg(nncostFunction, initial_nn_params, options);

% Obtain Theta1 and Theta2 back from nn_params
Theta1 = reshape(nn_params(1:hidden_layer_size * ...
                             (input_layer_size + 1)), ...
                 hidden_layer_size, (input_layer_size + 1));

Theta2 = reshape(nn_params((1 + (hidden_layer_size * ...
                                 (input_layer_size + 1))):end), ...
                 num_labels, (hidden_layer_size + 1));

displayData(Theta1(:, 2:end));
pred = predict(Theta1, Theta2, X);

fprintf('\nTraining Set Accuracy: %f\n', mean(double(pred == y)) * 100);

