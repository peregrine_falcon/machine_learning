function [J, grad] = costFunction(nn_params, ...
                                   input_layer_size, ...
                                   hidden_layer_size, ...
                                   num_labels, ...
                                   X, y, lambda)
  Theta1 = reshape(nn_params(1:hidden_layer_size * ...
                               (input_layer_size + 1)), ...
                   hidden_layer_size, (input_layer_size + 1));

  Theta2 = reshape(nn_params((1 + (hidden_layer_size * ...
                                   (input_layer_size + 1))):end), ...
                   num_labels, (hidden_layer_size + 1));

  m = size(X, 1);
  J = 0;
  Theta1_grad = zeros(size(Theta1));
  Theta2_grad = zeros(size(Theta2));

  % Cost Function
  % Convert y to yk form
  yk = zeros(size(y, 1), num_labels);
  for i = 1: num_labels
    y_part = y == i;
    yk(:, i) = y_part;
  endfor

  % Calculate the hypothesis
  Z2 = [ones(m, 1) X] * Theta1';
  A2 = sigmoid(Z2);
  Z3 = [ones(m, 1) A2] * Theta2';
  A3 = sigmoid(Z3);
  H = A3;

  % Calculate the cost
  % Note the additional summation to as we are adding all the terms of yk
  J = (1 / m) * (sum(sum(-yk .* log(H) ...
                         - (1 - yk) .* log(1 - H))))...
      + (lambda / (2 * m)) * (sum(sum(Theta1(:, 2:end) .^2))...
                              + sum(sum(Theta2(:, 2:end) .^2)));

  % Empty matrixes to store accumulated gradients
  Sigma3_Total = zeros(1, size(Theta2, 1));
  Sigma2_Total = zeros(1, size(Theta1, 1));

  for t = 1:m
    % Forward Propogation using random initial weights
    z2 = [1 X(t, :)] * Theta1';
    a2 = sigmoid(z2);
    z3 = [1, a2] * Theta2';
    a3 = sigmoid(z3);

    % Difference between output and trail FP as sigma3
    sigma3 = (a3 - yk(t, :));

    % Calculate sigma2 from sigma3
    sigma2 = (sigma3 * Theta2) .* (sigmoidGradient([1, z2]));

    % Accumulating the gradients for each loop
    Sigma2_Total = [Sigma2_Total; sigma2(:, 2:end)];
    Sigma3_Total = [Sigma3_Total; sigma3];

  endfor
  Delta_1 = Sigma2_Total(2:end, :)' * [ones(m,1) X];
  Delta_2 = Sigma3_Total(2:end, :)' * [ones(m,1) A2];

  Theta1_grad = Delta_1 ./ m + (lambda / m) * ...
                               [zeros(size(Theta1, 1), 1) Theta1(:, 2:end)];
  Theta2_grad = Delta_2 ./ m + (lambda / m) * ...
                               [zeros(size(Theta2, 1), 1) Theta2(:, 2:end)];

  % Unroll gradients
  grad = [Theta1_grad(:) ; Theta2_grad(:)];

endfunction

