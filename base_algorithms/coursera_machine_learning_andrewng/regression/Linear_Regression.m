% Implements Linear Regression

% Clear and Close all the windows and variables in Octave
clear; close all; clc;

% Linear Regression with single input variable

% Using data of a food truck company for targeting the next city for expansion
% Data contains Population versus Profits for various cities

% Load Data
data = load('Population_vs_Profits.txt');


% Assign input variables and output variables
X = data(:, 1); % Population in 10,0000s
y = data(:, 2); % Profits in 10,000s
m = size(X, 1); % number of training examples

% Plot y versus X to understand the data
plot(X, y, 'rx', 'MarkerSize', 10);
ylabel('Profits in 10,000');
xlabel('Population in 10,000');

% Cost and Gradient functions for Linear Regression

% Cost function
function J = computeCost(X, y, theta)
    m = size(X, 1);
    J = (1 / (2 * m)) * sum((X * theta - y) .^ 2);
endfunction

% Gradient Descent
function [theta, J_History] = gradientDescent(alpha, num_iters, X, y, theta)
    m = size(X, 1);
    J_History = zeros(num_iters, 1);

    for iter = 1:num_iters
        theta = theta - (alpha / m) .* (X' * (X * theta - y)); % Gradient Descent
        J_history(iter) = computeCost(X, y, theta);
    endfor
endfunction

% Normal Equation
function [theta] = normalEqn(X, y)
    theta = zeros(size(X, 2), 1);
    theta = pinv((X' * X)') * X' * y;
endfunction

% Perform Linear Regression
X = [ones(m, 1), X]; % Add a ones column to X
theta = zeros(2, 1); % Initialize theta
alpha = 0.01;
num_iters = 1500;

J = computeCost(X, y, theta)
[theta, J_History] = gradientDescent(alpha, num_iters, X, y, theta);

% Predict Output
predict1 = [1, 3.5] *theta;
fprintf('For population = 35,000, we predict a profit of %f\n',...
    predict1 * 10000);
predict2 = [1, 7] * theta;
fprintf('For population = 70,000, we predict a profit of %f\n',...
    predict2 * 10000);


% Plot the linear fit
hold on; % keep previous plot visible
plot(X(:,2), X*theta, '-')
legend('Training data', 'Linear regression')
hold off % don't overlay any more plots on this figure

% Plotting Cost function: Surface and Contour plots
fprintf('Visualizing J(theta_0, theta_1) ...\n')

% Grid over which we will calculate J
theta0_vals = linspace(-10, 10, 100);
theta1_vals = linspace(-4, 4, 100);

% initialize J_vals to a matrix of 0's
J_vals = zeros(length(theta0_vals), length(theta1_vals));

% Fill out J_vals
for i = 1:length(theta0_vals)
  for j = 1:length(theta1_vals)
	  t = [theta0_vals(i); theta1_vals(j)];
	  J_vals(i,j) = computeCost(X, y, t);
  end
end


% Because of the way meshgrids work in the surf command, we need to
% transpose J_vals before calling surf, or else the axes will be flipped
J_vals = J_vals';
% Surface plot
figure;
surf(theta0_vals, theta1_vals, J_vals)
xlabel('\theta_0'); ylabel('\theta_1');

% Contour plot
figure;
% Plot J_vals as 15 contours spaced logarithmically between 0.01 and 100
contour(theta0_vals, theta1_vals, J_vals, logspace(-2, 3, 20))
xlabel('\theta_0'); ylabel('\theta_1');
hold on;
plot(theta(1), theta(2), 'rx', 'MarkerSize', 10, 'LineWidth', 2);
