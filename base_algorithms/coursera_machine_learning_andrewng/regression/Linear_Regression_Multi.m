% Implements multi-variate Linear Regression

% Clear and Close all the windows and variables in Octave
clear; close all; clc;

% Linear Regression with multiple input variables

% Using data of Portland Housing Prices with two features involving
% 1. Size in sq. ft of the house and
% 2. Number of bedrooms
% Linear Regression is used to fit a model to predict the price of the house
% given the above two input features

% Load Data
data = load('Portland_Housing_Prices.txt');

% Assign input variables and output variables
X = data(:, 1:2); % Population in 10,0000s
y = data(:, 3); % Profits in 10,000s
m = length(y); % number of training examples


% Necessary functions to perform linear Regression

% Feature scaling
function [X_norm, mu, sigma] = featureNormalize(X)
  X_norm = X;
  mu = zeros(1, size(X, 2));
  sigma = zeros(1, size(X, 2));
  mu = mean(X);
  sigma = std(X);
  X_norm = (X - mu) ./ sigma;
endfunction

% Cost function
function J = computeCost(X, y, theta)
    m = length(y);

    % summation of the sum of squared differences
    J = (1 / (2 * m)) * sum((X * theta - y) .^ 2);
endfunction

% Gradient Descent
function [theta, J_History] = gradientDescent(alpha, num_iters, X, y, theta)
    m = length(y);
    J_History = zeros(num_iters, 1);

    for iter = 1:num_iters
        % reducing theta by a gradient term every iteration
        theta = theta - (alpha / m) .* (X' * (X * theta - y)); 

        J_History(iter) = computeCost(X, y, theta);
    endfor
endfunction

% Normal Equation
function [theta] = normalEqn(X, y)
    theta = zeros(size(X, 2), 1);

    % finding minima by equating the diffrentiation to zero
    theta = pinv((X' * X)') * X' * y;
endfunction

% plotting convergence paths
function plotConvergencePaths(alpha, num_iters, X, y, theta, color)
  [theta, J_History] = gradientDescent(alpha, num_iters, X, y, theta);
  plot(1:numel(J_History), J_History, [color '-'], 'LineWidth', 2);
  xlabel('Number of iterations');
  ylabel('Cost J');
endfunction

% Performing Linear Regression
[X_norm, mu, sigma] = featureNormalize(X);
X_norm = [ones(m, 1) X_norm];
theta = zeros(size(X_norm, 2), 1); % Initialize theta
alpha = 1.1;
num_iters = 400;

J = computeCost(X_norm, y, theta); 
[theta, J_History] = gradientDescent(alpha, num_iters, X_norm, y, theta);

% Compare training data with data shaped using linear regression model

% Plot for model trained using gradient descent 
figure;
scatter3(X_norm(:, 1), X_norm(:, 2), y);
xlabel('Normalized Area in Sq. ft');
ylabel('Normalized Number of rooms');
zlabel('Price of the House');
hold on;
scatter3(X_norm(:, 1), X_norm(:, 2), X_norm * theta);
legend('Training data', 'Linear regression')
hold off;

% Plot for model trained using normal equations
figure;
scatter3(X(:, 1), X(:, 2), y);
xlabel('Normalized Area in Sq. ft');
ylabel('Normalized Number of rooms');
zlabel('Price of the House');
hold on;
[theta] = normalEqn(X, y);
scatter3(X(:, 1), X(:, 2), X * theta);
legend('Training data', 'Linear regression')
hold off;

% Plot the different convergence graphs of gradient descent
figure;
theta_test = zeros(size(X_norm, 2), 1); % Initialize theta
plotConvergencePaths(0.01, 400, X_norm, y, theta_test, 'b');
hold on;
plotConvergencePaths(0.1, 400, X_norm, y, theta_test, 'r');
hold on;
plotConvergencePaths(1.0, 400, X_norm, y, theta_test, 'k');
hold on;
plotConvergencePaths(1.1, 400, X_norm, y, theta_test, 'g');
legend('alpha = 0.01', 'alpha = 0.1', 'alpha = 1.0', 'alpha = 1.1')
hold off;
