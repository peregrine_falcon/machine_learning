% Implement Neural Network Forward Propogation with trained weights


% Clear and Close all the windows and variables in Octave
clear; close all; clc;


% Load data
load('MNIST_Digit_Dataset.mat'); % training data stored in arrays X, y
load('TrainedNNweights.mat');    % training data stored in arrays X, y

input_layer_size  = 400;         % 20x20 Input Images of Digits
hidden_layer_size = 25;          % 25 hidden units
num_labels = 10;                 % 10 labels, from 1 to 10

m = size(X, 1);


% Randomly select 100 data points to display
rand_indices = randperm(m);
sel = X(rand_indices(1:100), :);


% Display data
displayData(sel);


% Necessary functions to perform Logistic Regression
function g = sigmoid(z)
  g = 1 ./ (1 + e .^ (-z));
endfunction


function p = predict(Theta1, Theta2, X)
  m = size(X, 1);
  num_labels = size(Theta2, 1);

  % You need to return the following variables correctly
  p = zeros(size(X, 1), 1);

  z2 = [ones(m, 1) X] * Theta1';
  a2 = sigmoid(z2);
  z3 = [ones(m, 1) a2] * Theta2';
  a3 = sigmoid(z3);
  [val, p] = max(a3, [], 2);

endfunction

pred = predict(Theta1, Theta2, X);
fprintf('\nTraining Set Accuracy: %f\n', mean(double(pred == y)) * 100);

%  Randomly permute examples
rp = randperm(m);

for i = 1:m
  % Display
  fprintf('\nDisplaying Example Image\n');
  displayData(X(rp(i), :));

  pred = predict(Theta1, Theta2, X(rp(i),:));
  fprintf('\nNeural Network Prediction: %d (digit %d)\n', pred, mod(pred, 10));

  % Pause with quit option
  s = input('Paused - press enter to continue, q to exit:','s');
  if s == 'q'
    break
  end
end

