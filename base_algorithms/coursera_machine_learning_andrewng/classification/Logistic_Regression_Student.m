% Implement Logistic Regression without regularization

% Clear and Close all the windows and variables in Octave
clear; close all; clc;

% Using data of Student test scores in two exams along with the details of
% admittance
% Logistic Regression is used to fit a model to classify whether a student gets
% admitted or not

% Load Data
data = load('Student_Test_Scores.txt');

% Assign input variables and output variables
X = data(:, 1:2); % Population in 10,0000s
y = data(:, 3); % Profits in 10,000s
m = length(y); % number of training examples

% Necessary functions to perform Logistic Regression
function plotData(X, y)
    figure;
    hold on;
    pos = find(y == 1);
    neg = find(y == 0);

    plot(X(pos, 1), X(pos, 2), 'k+', 'LineWidth', 2, 'MarkerSize', 7);
    plot(X(neg, 1), X(neg, 2), 'ko', 'MarkerFaceColor', 'y', 'MarkerSize', 7);

    xlabel('Exam 1 Score');
    ylabel('Exam 2 Score');
    legend('Admitted', 'Not Admitted');
    hold off;
endfunction

function g = sigmoid(z)
    g = 1 ./ (1 + e .^ (-z));
endfunction

function out = mapFeature(X1, X2, degree_var)
  % degree = degree_var
  out = ones(size(X1(:,1)));
  for i = 1:degree_var
    for j = 0:i
      out(:, end+1) = (X1.^(i-j)).*(X2.^j);
    end
  end
endfunction

function [J, grad] = costFunction(theta, X, y, regularization)
    m = length(y); % number of training examples
    J = 0;
    grad = zeros(size(theta));

    % without regularization
    if regularization = false
      % % cost function
      J = (1 / m) * (sum(-y .* log(sigmoid(X * theta)) ...
                      - (1 - y) .* log(1 - sigmoid(X * theta))));

      % gradient
      grad = (1 / m) * (X' * (sigmoid(X * theta) - y));

    % with regularization
    else
      lambda = 10000;
      % % cost function
      J = (1 / m) * (sum(-y .* log(sigmoid(X * theta)) ...
                         - (1 - y) .* log(1 - sigmoid(X * theta)))) ...
          + (lambda / (2 * m)) * sum(theta(2:end) .^ 2);

      % gradient
      grad = (1 / m) * (X' * (sigmoid(X * theta) - y)) + (lambda / m) * theta;
      grad(1) = grad(1) - (lambda / m) * theta(1);
    endif


endfunction

function plotDecisionBoundary(theta, X, y, degree_var)
  % Plot Data
  plotData(X(:,2:3), y);
  hold on
  if size(X, 2) <= 3
    % Only need 2 points to define a line, so choose two endpoints
    plot_x = [min(X(:,2))-2,  max(X(:,2))+2];

    % Calculate the decision boundary line
    plot_y = (-1./theta(3)).*(theta(2).*plot_x + theta(1));

    % Plot, and adjust axes for better viewing
    plot(plot_x, plot_y)

    % Legend, specific for the exercise
    legend('Admitted', 'Not admitted', 'Decision Boundary')
    axis([30, 100, 30, 100])
  else
    % Here is the grid range
    u = linspace(30, 100, 100);
    v = linspace(30, 100, 100);

    z = zeros(length(u), length(v));
    % Evaluate z = theta*x over the grid
    for i = 1:length(u)
      for j = 1:length(v)
        z(i,j) = mapFeature(u(i), v(j), degree_var)*theta;
      end
    end
    z = z'; % important to transpose z before calling contour

    % Plot z = 0
    % Notice you need to specify the range [0, 0]
    contour(u, v, z, [0, 0], 'LineWidth', 2)
  end
  hold off
endfunction


% Performing Classification

% Plot the training set
plotData(X, y)

% Initialization
degree = 3;
X = mapFeature(X(:, 1), X(:, 2), degree);
[m, n] = size(X);
initial_theta = zeros(n, 1);

% Optimize cost using fminunc and obtain optimum theta
[cost, grad] = costFunction(initial_theta, X, y, true);
options = optimset('GradObj', 'on', 'MaxIter', 400);

[theta, cost] = ...
fminunc(@(t)(costFunction(t, X, y)), initial_theta, options);

% Plotting decision boundary
plotDecisionBoundary(theta, X, y, degree)

% Probability
prob = sigmoid(mapFeature([45], [85], degree) * theta);
fprintf(['For a student with scores 45 and 85, we predict an admission ' ...
           'probability of %f\n'], prob);

p = zeros(m, 1);
H = sigmoid(X * theta);
for i = 1 : size(H, 1)
  if H(i) >= 0.5
    H(i) = 1;
  else
    H(i) = 0;
  endif
endfor
p = H;
fprintf('Train Accuracy: %f\n', mean(double(p == y)) * 100);

