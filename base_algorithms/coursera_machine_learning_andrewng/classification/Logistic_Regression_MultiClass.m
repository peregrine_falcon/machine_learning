% Implement Multi Class Classification


% Clear and Close all the windows and variables in Octave
clear; close all; clc;


% Load data
load('MNIST_Digit_Dataset.mat'); % training data stored in arrays X, y
m = size(X, 1);


% Randomly select 100 data points to display
rand_indices = randperm(m);
sel = X(rand_indices(1:100), :);


% Display data
displayData(sel);


% Necessary functions to perform Logistic Regression
function g = sigmoid(z)
  g = 1 ./ (1 + e .^ (-z));
endfunction

function [J, grad] = costFunction(theta, X, y, lambda)
  m = length(y); % number of training examples
  J = 0;
  grad = zeros(size(theta));

  J = (1 / m) * (sum(-y .* log(sigmoid(X * theta)) ...
                     - (1 - y) .* log(1 - sigmoid(X * theta)))) ...
                     + (lambda / (2 * m)) * sum(theta(2:end) .^ 2);

  grad = (1 / m) * (X' * (sigmoid(X * theta) - y)) + (lambda / m) * theta;
  grad(1) = grad(1) - (lambda / m) * theta(1);


  grad = grad(:);

endfunction

function [all_theta] = oneVsAll(X, y, num_labels, lambda)
  m = size(X, 1);
  n = size(X, 2);
  all_theta = zeros(num_labels, n + 1);
  X = [ones(m, 1) X];
  for i = 1: num_labels
    y_partial = y == i;
    initial_theta = zeros(size(X, 2), 1);
    options = optimset('GradObj', 'on', 'MaxIter', 100);
    [theta_partial, cost] = ...
        fmincg (@(t)(costFunction(t, X, y_partial, lambda)), ...
                                      initial_theta, options);
    all_theta(i, :) = theta_partial';
  endfor
endfunction

function p = predictOneVsAll(all_theta, X)
  m = size(X, 1);
  num_labels = size(all_theta, 1);

  p = zeros(size(X, 1), 1);
  X = [ones(m, 1) X];
  H = sigmoid(X * all_theta');
  [val, p] = max(H, [], 2);

endfunction

% Multi-class classification
lambda = 0.1;
num_labels = 10;
[all_theta] = oneVsAll(X, y, num_labels, lambda);

pred = predictOneVsAll(all_theta, X);
fprintf('\nTraining Set Accuracy: %f\n', mean(double(pred == y)) * 100);

% Predicted output for the displayed output grid
% out = pred(rand_indices(1:100), :);

% for i=1:length(out)
%   if out(i) == 10
%     out(i) = 0;
%   endif
% endfor

% classification_out = reshape(out, 10 ,10)'
% y_out = reshape(y(rand_indices(1:100), :), 10 ,10)'
% classification_out == y_out
