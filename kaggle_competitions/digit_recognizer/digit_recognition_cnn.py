import itertools

import numpy as np
import pandas as pd
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split

import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import seaborn as sns
from keras.callbacks import ReduceLROnPlateau
from keras.layers import Conv2D, Dense, Dropout, Flatten, MaxPool2D
from keras.models import Sequential
from keras.optimizers import RMSprop
from keras.preprocessing.image import ImageDataGenerator
from keras.utils.np_utils import to_categorical

np.random.seed(2)

sns.set()

# Load data
train = pd.read_csv('train.csv')
test = pd.read_csv('test.csv')

Y_train = train["label"]
X_train = train.drop(labels=["label"], axis=1)

# Plot counts
# g = sns.countplot(Y_train)
print(Y_train.value_counts())
# plt.show()

# Check for null values
print(X_train.isnull().any().describe())
print(test.isnull().any().describe())

# Normalization
X_train = X_train / 255.0
test = test / 255.0

# Reshape
X_train = X_train.values.reshape(-1, 28, 28, 1)
test = test.values.reshape(-1, 28, 28, 1)

# Label Encoding
Y_train = to_categorical(Y_train, num_classes=10)

# Split training and validation set
random_seed = 2
X_train, X_val, Y_train, Y_val = train_test_split(X_train,
                                                  Y_train,
                                                  test_size=0.1,
                                                  random_state=random_seed)

c = plt.imshow(X_train[0][:, :, 0])
plt.show()
