import sys
import warnings

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from sklearn.ensemble import RandomForestRegressor
from sklearn.linear_model import Lasso
from sklearn.metrics import mean_absolute_error
from sklearn.model_selection import train_test_split
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import LabelEncoder, RobustScaler
from xgboost import XGBRegressor

# Supress all warnings for this application
if not sys.warnoptions:
    warnings.simplefilter("ignore")

# Graph properties
sns.set_style("whitegrid")
plt.style.use(style="ggplot")
plt.rcParams["figure.figsize"] = (10, 6)


def null_check(data):
    null_test = pd.DataFrame(data.isnull().sum().sort_values(ascending=False))
    null_test.columns = ["Null_Count"]
    print(null_test[null_test["Null_Count"] > 0], "\n")


def dominating_cols(data):
    if dominating_cols == True:
        print(
            data.select_dtypes("object")
            .describe()
            .loc["freq"]
            .sort_values(ascending=False)
            / len(data)
        )


def one_hot(columns_list):
    global all_data
    for col in columns_list:
        data_encoded = pd.get_dummies(all_data[col], prefix=col)
        all_data = pd.merge(all_data, data_encoded, on="Id")
        all_data = all_data.drop(columns=col)
    # print(all_data.shape)


def log_transform(columns_list):
    transformed_columns = []
    for col in columns_list:
        if all_data[col].skew() > 0.5:
            all_data[col] = np.log1p(all_data[col])
            transformed_columns.append(col)
    # print(f"{len(transformed_columns)} columns have been transformed")
    # print(all_data.shape)


# print new line to tab in output
print("\n")

# Read data
train_data_path = "../../data/housing_prices_competition/train.csv"
test_data_path = "../../data/housing_prices_competition/test.csv"

train_data = pd.read_csv(train_data_path, index_col="Id")
test_data = pd.read_csv(test_data_path, index_col="Id")


# Data Exploration and Wrangling

# Target skew
# print("Skew: ", train_data.SalePrice.skew())
# plt.hist(train_data.SalePrice, color="blue")
# plt.xlabel("SalePrice")
# plt.title("Distribution of SalePrice")
# plt.show()

# print("Skew of log: ", np.log1p(train_data.SalePrice).skew())
# plt.hist(target, color="blue")
# plt.xlabel("SalePrice")
# plt.title("Distribution of SalePrice")
# plt.show()


# Correlation
numerical_columns = train_data.select_dtypes(exclude="object")
correlation = numerical_columns.corr()
# print(
#     "Highest Correlation: \n",
#     correlation["SalePrice"].sort_values(ascending=False)[1:11],
#     "\n",
# )
# print(
#     "Lowest Correlation: \n",
#     correlation["SalePrice"].sort_values(ascending=False)[-10:],
#     "\n",
# )

# Check correlation by columns

# 1. OverallQual

# print(train_data.OverallQual.value_counts().sort_values(ascending=False))
# def pivot_plot(data, variable, onVariable, aggregate_function):
#     pivot_varible = data.pivot_table(
#         index=variable, values=onVariable, aggfunc=aggregate_function
#     )
#     pivot_varible.plot(kind="bar", color="blue")
#     plt.xlabel(variable)
#     plt.ylabel(onVariable)
#     plt.xticks(rotation=0)
#     plt.show()


# pivot_plot(train_data, "OverallQual", "SalePrice", np.median)

# 2. GrLivArea
# _ = sns.regplot(train_data.GrLivArea, train_data.SalePrice)
# plt.show()

train_data = train_data.drop(
    train_data[
        (train_data["GrLivArea"] > 4000) & (train_data["SalePrice"] < 200000)
    ].index
)

# _ = sns.regplot(train_data.GrLivArea, train_data.SalePrice)
# plt.show()

# 3. GarageCars
# pivot_plot(train_data, "GarageCars", "SalePrice", np.median)
# plt.show()

# 4. GarageArea
# _ = sns.regplot(train_data.GarageArea, train_data.SalePrice)
# plt.show()

train_data = train_data.drop(train_data[train_data["GarageArea"] > 1200].index)
# _ = sns.regplot(train_data.GarageArea, train_data.SalePrice)
# plt.show()

# DataFrame for SalePrice and logSalePrice
train_data["logSalePrice"] = np.log1p(train_data["SalePrice"])
sale_price = train_data[["SalePrice", "logSalePrice"]]

train_data = train_data.drop(columns=["SalePrice", "logSalePrice"])

all_data = pd.concat((train_data, test_data))
# print(all_data.shape)


# Handling Categorical Features with null values
categorical_null_features = pd.DataFrame(
    all_data.select_dtypes("object").isnull().sum().sort_values(ascending=False)
)
categorical_null_features.columns = ["Null_Count"]
categorical_null_features.index.name = "Feature"
categorical_null_features = categorical_null_features[
    categorical_null_features["Null_Count"] > 0
]

# print((categorical_null_features / len(all_data)) * 100, "\n")
# for col in categorical_null_features.index:
#     print(col, all_data[col].count())
#     print(all_data[col].value_counts())
#     print("\n")

# print(all_data.loc[all_data["PoolArea"] > 0, ["PoolArea", "PoolQC"]])
columns_to_none = [
    "PoolQC",
    "MiscFeature",
    "Alley",
    "Fence",
    "FireplaceQu",
    "GarageCond",
    "GarageQual",
    "GarageFinish",
    "GarageType",
    "BsmtCond",
    "BsmtExposure",
    "BsmtQual",
    "BsmtFinType2",
    "BsmtFinType1",
    "MasVnrType",
]

for col in columns_to_none:
    all_data[col] = all_data[col].fillna("None")

columns_to_mode = [
    "MSZoning",
    "Functional",
    "Utilities",
    "Electrical",
    "KitchenQual",
    "SaleType",
    "Exterior2nd",
    "Exterior1st",
]

for col in columns_to_mode:
    all_data[col] = all_data[col].fillna(all_data[col].mode()[0])

# Handling Numerical Features with null columns
numerical_null_features = pd.DataFrame(
    all_data.select_dtypes(exclude="object").isnull().sum().sort_values(ascending=False)
)
numerical_null_features.columns = ["Null_Count"]
numerical_null_features.index.name = "Feature"
numerical_null_features = numerical_null_features[
    numerical_null_features["Null_Count"] > 0
]

# print((numerical_null_features / len(all_data)) * 100, "\n")
# for col in numerical_null_features.index:
#     print(col, all_data[col].count())
#     print(all_data[col].value_counts())
#     print("\n")

columns_to_zero = [
    "GarageYrBlt",
    "MasVnrArea",
    "BsmtHalfBath",
    "BsmtFullBath",
    "GarageArea",
    "BsmtFinSF1",
    "BsmtFinSF2",
    "BsmtUnfSF",
    "TotalBsmtSF",
    "GarageCars",
]

for col in columns_to_zero:
    all_data[col] = all_data[col].fillna(0)

# _ = sns.regplot(train_data.LotFrontage, sale_price.SalePrice)
# plt.show()

all_data["LotFrontage"] = all_data.groupby("Neighborhood")["LotFrontage"].apply(
    lambda x: x.fillna(x.median())
)

# null_test = pd.DataFrame(all_data.isnull().sum().sort_values(ascending=False))
# null_test.columns = ["Null_Count"]
# print(null_test[null_test["Null_Count"] > 0])

# print(all_data.shape)

# New features

# 1. TotalSF
# figure, ((ax1, ax2), (ax3, ax4)) = plt.subplots(nrows=2, ncols=2)
# _ = sns.regplot(train_data["TotalBsmtSF"], sale_price["SalePrice"], ax=ax1)
# _ = sns.regplot(train_data["1stFlrSF"], sale_price["SalePrice"], ax=ax2)
# _ = sns.regplot(train_data["2ndFlrSF"], sale_price["SalePrice"], ax=ax3)
# _ = sns.regplot(
#     train_data["TotalBsmtSF"] + train_data["1stFlrSF"] + train_data["2ndFlrSF"],
#     sale_price["SalePrice"],
#     ax=ax4,
# )
# plt.show()

all_data["TotalSF"] = (
    all_data["TotalBsmtSF"] + all_data["1stFlrSF"] + all_data["2ndFlrSF"]
)

# 2. TotalBath
# figure, ((ax1, ax2), (ax3, ax4)) = plt.subplots(nrows=2, ncols=2)
# _ = sns.barplot(train_data["BsmtFullBath"], sale_price["SalePrice"], ax=ax1)
# _ = sns.barplot(train_data["BsmtHalfBath"], sale_price["SalePrice"], ax=ax2)
# _ = sns.barplot(train_data["FullBath"], sale_price["SalePrice"], ax=ax3)
# _ = sns.barplot(
#     train_data["BsmtFullBath"]
#     + (0.5 * train_data["BsmtHalfBath"])
#     + (0.5 * train_data["HalfBath"])
#     + train_data["FullBath"],
#     sale_price["SalePrice"],
#     ax=ax4,
# )
# plt.show()

all_data["TotalBath"] = (
    all_data["BsmtFullBath"]
    + (0.5 * all_data["BsmtHalfBath"])
    + all_data["FullBath"]
    + 0.5 * (all_data["HalfBath"])
)

# 3. YearBuilt + YearRemodAdd

# figure, (ax1, ax2, ax3) = plt.subplots(nrows=1, ncols=3)
# figure.set_size_inches(18, 8)
# _ = sns.regplot(train_data["YearBuilt"], sale_price["SalePrice"], ax=ax1)
# _ = sns.regplot(train_data["YearRemodAdd"], sale_price["SalePrice"], ax=ax2)
# _ = sns.regplot(
#     (train_data["YearBuilt"] + train_data["YearRemodAdd"]) / 2,
#     sale_price["SalePrice"],
#     ax=ax3,
# )
# plt.show()

all_data["YrBlt&RemodAdd"] = (all_data["YearBuilt"] + all_data["YearRemodAdd"]) / 2


# 4. Other fields
all_data["HasPool"] = all_data["PoolArea"].apply(lambda x: 1 if x > 0 else 0)
all_data["HasGarage"] = all_data["GarageArea"].apply(lambda x: 1 if x > 0 else 0)
all_data["HasBsmt"] = all_data["TotalBsmtSF"].apply(lambda x: 1 if x > 0 else 0)


# Treat some numerical data as categorical
# all_data["MSSubClass"] = all_data["MSSubClass"].astype(str)
# all_data["MoSold"] = all_data["MoSold"].astype(str)
# all_data["YrSold"] = all_data["YrSold"].astype(str)


# Label encoding values that are in heirarchy

# Basement related fields
Bsmt = all_data.filter(regex="Bsmt").copy()

bsmt_qual_encoder = LabelEncoder()
Bsmt["BsmtQual"] = bsmt_qual_encoder.fit_transform(Bsmt["BsmtQual"])

bsmt_cond_encoder = LabelEncoder()
Bsmt["BsmtCond"] = bsmt_cond_encoder.fit_transform(Bsmt["BsmtCond"])

bsmt_exposure_encoder = LabelEncoder()
Bsmt["BsmtExposure"] = bsmt_exposure_encoder.fit_transform(Bsmt["BsmtExposure"])

bsmt_fint1_encoder = LabelEncoder()
Bsmt["BsmtFinType1"] = bsmt_fint1_encoder.fit_transform(Bsmt["BsmtFinType1"])

bsmt_fint2_encoder = LabelEncoder()
Bsmt["BsmtFinType2"] = bsmt_fint2_encoder.fit_transform(Bsmt["BsmtFinType2"])

Bsmt["BsmtScore"] = Bsmt["BsmtQual"] * Bsmt["BsmtCond"] * Bsmt["TotalBsmtSF"]
Bsmt["BsmtFinScore"] = (
    Bsmt["BsmtFinType1"] * Bsmt["BsmtFinSF1"]
    + Bsmt["BsmtFinType2"] * Bsmt["BsmtFinSF2"]
)

# _ = sns.regplot(Bsmt["BsmtScore"][:1454], sale_price["SalePrice"])
# plt.show()

all_data["BsmtScore"] = Bsmt["BsmtScore"]
all_data["BsmtFinScore"] = Bsmt["BsmtFinScore"]

# Garage related fields
Garage = all_data.filter(regex="Garage").copy()

garage_finish_encoder = LabelEncoder()
Garage["GarageFinish"] = garage_finish_encoder.fit_transform(Garage["GarageFinish"])

garage_quality_encoder = LabelEncoder()
Garage["GarageQual"] = garage_quality_encoder.fit_transform(Garage["GarageQual"])

garage_condition_encoder = LabelEncoder()
Garage["GarageCond"] = garage_condition_encoder.fit_transform(Garage["GarageCond"])

garage_type_encoder = LabelEncoder()
Garage["GarageType"] = garage_type_encoder.fit_transform(Garage["GarageType"])

Garage["GarageScore"] = (
    Garage["GarageArea"]
    * Garage["GarageFinish"]
    * Garage["GarageQual"]
    * Garage["GarageCond"]
    * Garage["GarageType"]
)

# _ = sns.regplot(Garage["GarageScore"][:1454], sale_price["logSalePrice"])
# plt.show()

all_data["GarageScore"] = Garage["GarageScore"]

# Dropping features with dominating values > 97%
dominating_cols_drop = [
    "Utilities",
    "PoolQC",
    "Street",
    "Condition2",
    "RoofMatl",
    "Heating",
    # Columns to drop, due to large number of null values. Not related to above
    "MiscFeature",
    "MiscVal",
    # "PoolArea",
]

all_data = all_data.drop(columns=dominating_cols_drop)


one_hot(list(all_data.select_dtypes("object").columns))


log_transform(list(all_data.select_dtypes(exclude="object")))


# Model fitting
train = all_data[: len(train_data)]
test = all_data[len(train_data) :]

X = train.copy()
y = sale_price["logSalePrice"].copy()

# print(X.head())

X_train, X_valid, y_train, y_valid = train_test_split(X, y, random_state=42)


# XGB Regressor
xgb_model = XGBRegressor(
    n_estimators=1000, learning_rate=0.05, objective="reg:squarederror"
)
xgb_model.fit(
    X_train,
    y_train,
    eval_set=[(X_valid, y_valid)],
    early_stopping_rounds=10,
    verbose=False,
)

xgb_log_pred = xgb_model.predict(X_valid)
xgb_pred = np.exp(xgb_log_pred) - 1

print("XGB MAE: ", mean_absolute_error(xgb_pred, y_valid))

# Lasso model
lasso_model = Lasso(alpha=0.00035, random_state=42)
lr_lasso = make_pipeline(RobustScaler(), lasso_model)
lr_lasso.fit(X_train, y_train)
lasso_log_pred = lr_lasso.predict(X_valid)
lasso_pred = np.exp(lasso_log_pred) - 1

print("Lasso MAE: ", mean_absolute_error(lasso_pred, y_valid))


# Submission Lasso
log_pred = lr_lasso.predict(test)
pred = np.exp(log_pred) - 1

submission = pd.read_csv("../../data/housing_prices_competition/sample_submission.csv")
submission["SalePrice"] = pred
submission.to_csv("./output/lasso.csv", index=False)

# Submission XGB
log_pred = xgb_model.predict(test)
pred = np.exp(log_pred) - 1

submission = pd.read_csv("../../data/housing_prices_competition/sample_submission.csv")
submission["SalePrice"] = pred
submission.to_csv("./output/xgboost.csv", index=False)

# Submission Blend
log_pred = xgb_model.predict(test) * 0.3 + lr_lasso.predict(test) * 0.7
pred = np.exp(log_pred) - 1

submission = pd.read_csv("../../data/housing_prices_competition/sample_submission.csv")
submission["SalePrice"] = pred
submission.to_csv("./output/blend.csv", index=False)
