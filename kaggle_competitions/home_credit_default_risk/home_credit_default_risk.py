#! /home/falcon/Applications/anaconda/bin/python3.7
import numpy as np
import pandas as pd

train_data_path = (
    "~/Documents/machine_learning/data/home-credit-default-risk/application_train.csv"
)
test_data_path = (
    "~/Documents/machine_learning/data/home-credit-default-risk/application_test.csv"
)

app_train = pd.read_csv(train_data_path)
app_test = pd.read_csv(test_data_path)

print(app_train.shape)
