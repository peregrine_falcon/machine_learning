#! /home/falcon/Applications/anaconda/bin/python3.7

import matplotlib.pyplot as plt
import numpy as np

import pandas as pd

print("\n")
data_path = "~/Documents/machine_learning/data/winemag_data/winemag-data_first150k.csv"
reviews = pd.read_csv(data_path)

# # Plot bar graph of value counts of top 15 provinces
# plt.figure(figsize=(15, 6))
# plt.bar(np.arange(15), reviews.province.value_counts().head(15).values, width=0.35)
# plt.xlabel("Provinces")
# plt.ylabel("Value Counts")
# plt.xticks(np.arange(15), reviews.province.value_counts().head(15).index, rotation=45)
# plt.tight_layout()
# plt.show()

# # Plot bar graph of percentage of wine reviews for top 15 provinces
# plt.figure(figsize=(15, 6))
# plt.bar(
#     np.arange(15),
#     (reviews.province.value_counts().head(15).values / reviews.shape[0]),
#     width=0.35,
# )
# plt.xlabel("Provinces")
# plt.ylabel("Value Counts")
# plt.xticks(np.arange(15), reviews.province.value_counts().head(15).index, rotation=45)
# plt.tight_layout()
# plt.show()

# Plot bar graph of percentage of wine reviews for top 15 provinces
ind = np.arange(reviews.points.value_counts().shape[0])
# plt.figure(figsize=(len(ind) / 2, 6))
plt.bar(ind, reviews.points.value_counts().sort_index(), width=0.6)
# plt.xlabel("Provinces")
# plt.ylabel("Value Counts")
plt.yticks(ind, reviews.points.value_counts().index, rotation=90)
# plt.tight_layout()
plt.show()
