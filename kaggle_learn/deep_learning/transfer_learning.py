import os

import matplotlib.image as img
import matplotlib.pyplot as plt
import numpy as np
from tensorflow.python.keras.applications import ResNet50
from tensorflow.python.keras.applications.resnet50 import preprocess_input
from tensorflow.python.keras.layers import (Dense, Flatten,
                                            GlobalAveragePooling2D)
from tensorflow.python.keras.models import Sequential
from tensorflow.python.keras.preprocessing.image import ImageDataGenerator


def abs_image_paths(directory):
    for dir_path, dir_name, file_names in os.walk(directory):
        for f in file_names:
            yield os.path.abspath(os.path.join(dir_path, f))


print("\n")

train_rural_image_directory = (
    "/home/falcon/Documents/machine_learning/data/rural_and_urban_photos/train/rural"
)

train_urban_image_directory = (
    "/home/falcon/Documents/machine_learning/data/rural_and_urban_photos/train/urban"
)

train_urban_image_paths = list(abs_image_paths(train_urban_image_directory))
train_rural_image_paths = list(abs_image_paths(train_rural_image_directory))

# for i in train_rural_image_paths[-5:]:
#     image = img.imread(i)
#     img_plt = plt.imshow(image)
#     plt.show()

num_classes = 2
resnet50_weights_path = (
    "/home/falcon/Documents/machine_learning/data/RestNet50_Weights"
    + "/resnet50_weights_tf_dim_ordering_tf_kernels_notop.h5"
)

# Specify model
my_model = Sequential()
my_model.add(ResNet50(include_top=False, pooling="avg", weights=resnet50_weights_path))
my_model.add(Dense(num_classes, activation="softmax"))

my_model.layers[0].trainable = False

# Compile model
my_model.compile(
    optimizer="sgd",
    loss="categorical_crossentropy",
    # loss="sparse_categorical_crossentropy",
    metrics=["accuracy"],
)

# Fit model
image_size = 224
data_generator = ImageDataGenerator(
    preprocessing_function=preprocess_input, rescale=(1 / (225 / 2))
)

train_generator = data_generator.flow_from_directory(
    "/home/falcon/Documents/machine_learning/data/rural_and_urban_photos/train",
    target_size=(image_size, image_size),
    batch_size=10,
    class_mode="categorical",
    # class_mode="binary",
)

validation_generator = data_generator.flow_from_directory(
    "/home/falcon/Documents/machine_learning/data/rural_and_urban_photos/val",
    target_size=(image_size, image_size),
    class_mode="categorical",
)

my_model.fit_generator(
    train_generator,
    steps_per_epoch=3,
    validation_data=validation_generator,
    validation_steps=1,
)

x, y = train_generator.next()
for i in range(len(x)):
    image = x[i]
    # plt.imshow(image)
    # plt.imshow((image * 255).astype(np.uint8))
    if y[i][0] == 1:
        # plt.title("rural")
        print("rural")
    else:
        # plt.title("urban")
        print("urban")
    # plt.show()
print(my_model.predict(train_generator.next()))
