import csv
import os
import sys
import warnings
from os.path import join

import matplotlib.image as img
import matplotlib.pyplot as plt
import numpy as np
from tensorflow.python.keras.applications import ResNet50
from tensorflow.python.keras.applications.resnet50 import (decode_predictions,
                                                           preprocess_input)
from tensorflow.python.keras.preprocessing.image import img_to_array, load_img

# Supress all warnings
if not sys.warnoptions:
    warnings.simplefilter("ignore")


# Functions
def abs_file_paths(directory):
    for dir_path, _, file_name in os.walk(directory):
        for f in file_name:
            yield os.path.abspath(os.path.join(dir_path, f))


def read_and_prep_images(image_paths):
    image_size = 224
    image_height = image_size
    image_width = image_size
    images = [
        load_img(image_path, target_size=(image_height, image_width))
        for image_path in image_paths
    ]
    img_array = np.array([img_to_array(img) for img in images])
    output = preprocess_input(img_array)
    return output


def resnet50_predictions(image_paths, weights_path):
    model = ResNet50(weights=weights_path)
    test_data = read_and_prep_images(image_paths)
    preds = model.predict(test_data)
    return preds


def sample_model_predictions(preds, image_paths):
    # most likely labels
    decoded = [l[0] for l in decode_predictions(preds, top=1)]
    labels = [i[1] for i in decoded]

    for i, image_path in enumerate(image_paths):
        image = img.imread(image_path)
        img_plt = plt.imshow(image)
        plt.title(
            str(labels[i]).lower()
            # + " " + str(id_to_label().get(image_path[76:-4]))
        )
        plt.show()


def id_to_label(
    file_path="/home/falcon/Documents/machine_learning/data/dog-breed-identification/labels.csv"
):
    dog_breed_labels = {}
    with open(file_path) as f:
        csv_reader = csv.reader(f)
        line_count = 0
        for row in csv_reader:
            if line_count > 0:
                dog_breed_labels[row[0]] = row[1]
            line_count += 1

        return dog_breed_labels


def calc_accuracy(preds, image_paths):
    correct_labels = id_to_label()
    is_correct = []
    decoded = [l[0] for l in decode_predictions(preds, top=1)]
    labels = [i[1] for i in decoded]

    for i, image_path in enumerate(image_paths):
        if labels[i].lower() == correct_labels.get(image_path[76:-4]).lower():
            is_correct.append(True)
        else:
            is_correct.append(False)
    return sum(is_correct) / len(image_paths)


# weights paths
weights_file_path = "/home/falcon/Documents/machine_learning/data/RestNet50_Weights/"
weights_file_name = "resnet50_weights_tf_dim_ordering_tf_kernels.h5"
weights_path = join(weights_file_path, weights_file_name)

# data paths
# train_dog_paths = list(abs_file_paths("../../data/dog-breed-identification/train/"))
train_dog_paths = list(
    abs_file_paths(
        "/home/falcon/Documents/machine_learning/data/dog-breed-identification/train"
    )
)
test_dog_paths = list(
    abs_file_paths(
        "/home/falcon/Documents/machine_learning/data/dog-breed-identification/train"
    )
)

image_paths = train_dog_paths[:15]

# print(image_paths)
# sample_model_predictions(resnet50_predictions(image_paths, weights_path), image_paths)
print(calc_accuracy(resnet50_predictions(image_paths, weights_path), image_paths))
