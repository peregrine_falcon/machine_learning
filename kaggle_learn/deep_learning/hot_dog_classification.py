import os
from os.path import join

import matplotlib.image as img
import matplotlib.pyplot as plt
import numpy as np
from tensorflow.keras.applications import ResNet50
from tensorflow.keras.applications.resnet50 import (decode_predictions,
                                                    preprocess_input)
from tensorflow.keras.preprocessing.image import img_to_array, load_img


# Functions
def abs_file_paths(directory):
    for dir_path, _, file_name in os.walk(directory):
        for f in file_name:
            yield os.path.abspath(os.path.join(dir_path, f))


def read_and_prep_images(image_paths, image_height=image_size, image_width=image_size):
    # load images
    images = [
        load_img(image_path, target_size=(image_height, image_width))
        for image_path in image_paths
    ]
    # convert images into np arrays
    image_array = np.array([img_to_array(image) for image in images])
    output = preprocess_input(image_array)
    return output


def sample_model_predictions(preds, image_paths=image_paths):
    # most likely labels
    decoded = [l[0] for l in decode_predictions(preds, top=1)]
    labels = [i[1] for i in decoded]

    for i, image_path in enumerate(image_paths):
        image = img.imread(image_path)
        img_plt = plt.imshow(image)
        plt.title(labels[i])
        plt.show()


def is_hot_dog(preds):
    # most likely labels
    decoded = [l[0] for l in decode_predictions(preds, top=1)]
    labels = [i[1] for i in decoded]
    is_hotdog = [i == "hotdog" for i in labels]
    return is_hotdog


def calc_accuracy(model, paths_to_hotdog, paths_to_other):
    num_hot_dog_imgs = len(paths_to_hotdog)
    num_other_imgs = len(paths_to_other)

    hot_dog_image_data = read_and_prep_images(paths_to_hotdog)
    hot_dog_predictions = model.predict(hot_dog_image_data)

    correct_hotdog_predictions = sum(is_hot_dog(hot_dog_predictions))

    other_image_data = read_and_prep_images(paths_to_other)
    other_image_predictions = model.predict(other_image_data)

    correct_other_predictions = num_other_imgs - sum(
        is_hot_dog(other_image_predictions)
    )

    total_correct = correct_hotdog_predictions + correct_other_predictions
    total_predictions = num_hot_dog_imgs + num_other_imgs

    return total_correct / total_predictions


def resnet50_predictions(image_paths=image_paths, weights_path=weights_path):
    model = ResNet50(weights=weights_path)
    test_data = read_and_prep_images(image_paths)
    preds = model.predict(test_data)
    return preds


# Model weights
weights_file_path = "/home/falcon/Documents/machine_learning/data/RestNet50_Weights/"
weights_file_name = "resnet50_weights_tf_dim_ordering_tf_kernels.h5"
weights_path = join(weights_file_path, weights_file_name)

train_hotdog_paths = list(
    abs_file_paths("../../data/hot-dog-not-hot-dog/train/hot_dog")
)

train_other_paths = list(
    abs_file_paths("../../data/hot-dog-not-hot-dog/train/not_hot_dog")
)

# sample hot dog image paths
hot_dog_img_paths = train_hotdog_paths[:10]

# sample other image paths
other_img_paths = train_other_paths[:10]

image_paths = hot_dog_img_paths + other_img_paths
image_size = 224

# sample_model_predictions(resnet50_predictions(), hot_dog_img_paths)

print(
    "ResNet50 Model Accuracy: ",
    calc_accuracy(
        ResNet50(weights=weights_path),
        hot_dog_img_paths,
        other_img_paths
        # train_hotdog_paths, train_other_paths
    ),
)
