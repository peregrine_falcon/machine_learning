import sys
import warnings
from os.path import abspath, join

import matplotlib.image as img
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
from tensorflow.python import keras
from tensorflow.python.keras.layers import Conv2D, Dense, Dropout, Flatten
from tensorflow.python.keras.models import Sequential

# Supress all warnings for this application
if not sys.warnoptions:
    warnings.simplefilter("ignore")

# Graph properties
sns.set_style("whitegrid")
plt.style.use(style="ggplot")
print("\n")


def prep_data():

    # train data path
    train_data_dir = "/home/falcon/Documents/machine_learning/data/digit_recognizer/"
    train_file_name = "train.csv"
    train_data_path = abspath(join(train_data_dir, train_file_name))

    # test data_path
    test_data_dir = "/home/falcon/Documents/machine_learning/data/digit_recognizer/"
    test_file_name = "test.csv"
    test_data_path = abspath(join(test_data_dir, test_file_name))

    train_data = pd.read_csv(train_data_path)
    test_data = pd.read_csv(test_data_path)

    y, label = train_data["label"], train_data["label"]
    X = train_data.drop(columns=["label"], axis=1)
    del train_data

    # normalization
    X = X / 225
    test_data = test_data / 225

    # reshape [28 pixels, 28 pixels, 1 channel]
    X = X.values.reshape(-1, 28, 28, 1)
    test_data = test_data.values.reshape(-1, 28, 28, 1)

    # one hot encoding of output
    y = keras.utils.to_categorical(y)

    return X, y, label, test_data


def display_image(X, label, preds, number=10):
    # show a sample image
    for i in range(number):
        image = X[i][:, :, 0]
        plt.imshow(image, cmap="gray_r")
        plt.title(f"Actual: {label[i]}, Predicted: {preds[i]}")
        plt.show()


# CNN Model
def CNN_model(X_train, X_valid, y_train, y_valid):

    model = Sequential()
    model.add(
        Conv2D(
            20,
            kernel_size=(3, 3),
            activation="relu",
            input_shape=(img_rows, img_cols, 1),
        )
    )

    # hidden layers
    model.add(Conv2D(20, kernel_size=(3, 3), activation="relu"))
    model.add(Flatten())
    model.add(Dense(128, activation="relu"))

    # output layer
    model.add(Dense(num_classes, activation="softmax"))

    model.compile(
        loss=keras.losses.categorical_crossentropy,
        optimizer="adam",
        metrics=["accuracy"],
    )

    model.fit(
        X_train, y_train, batch_size=128, epochs=2, validation_data=(X_valid, y_valid)
    )

    return model


def submission(model, test):
    submission_data_dir = (
        "/home/falcon/Documents/machine_learning/data/digit_recognizer/"
    )
    submission_file_name = "sample_submission.csv"
    submission_file_path = abspath(join(submission_data_dir, submission_file_name))
    submission = pd.read_csv(submission_file_path)

    preds = model.predict(test)
    preds = np.argmax(preds, axis=1)

    submission["Label"] = preds
    submission.to_csv("./submission.csv", index=False)


# data
X, y, label, test = prep_data()

# split data for training and cross-validation
X_train, X_valid, y_train, y_valid = train_test_split(
    X, y, train_size=0.8, test_size=0.2, random_state=42
)

model = CNN_model(X_train, X_valid, y_train, y_valid)

train_preds = model.predict(X_valid)

# display_image(X, label, train_preds)
Y_true = np.argmax(y_valid, axis=1)
Y_pred = np.argmax(train_preds, axis=1)

# _ = sns.heatmap(confusion_matrix(Y_true, Y_pred), annot=True, fmt="d")
# plt.show()

submission(model, test)
