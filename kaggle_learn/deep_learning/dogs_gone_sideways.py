from itertools import islice

import matplotlib.image as img
import matplotlib.pyplot as plt
import numpy
from tensorflow.python.keras.applications import ResNet50
from tensorflow.python.keras.applications.resnet50 import preprocess_input
from tensorflow.python.keras.layers import (Dense, Flatten,
                                            GlobalAveragePooling2D)
from tensorflow.python.keras.models import Sequential
from tensorflow.python.keras.preprocessing.image import ImageDataGenerator

print("\n")

# Model Specification
num_classes = 2
resnet50_weights_path = (
    "/home/falcon/Documents/machine_learning/data/RestNet50_Weights"
    + "/resnet50_weights_tf_dim_ordering_tf_kernels_notop.h5"
)

model = Sequential()
model.add(ResNet50(include_top=False, pooling="avg", weights=resnet50_weights_path))
model.add(Dense(num_classes, activation="softmax"))

model.layers[0].trainable = False

# Model Compilation
model.compile(optimizer="sgd", loss="categorical_crossentropy", metrics=["accuracy"])

# Model Fitting
image_size = 224

# Also perform data augmentation
data_generator = ImageDataGenerator(
    preprocessing_function=preprocess_input,
    # rescale=(1 / 225),
    # horizontal_flip=True,
    # width_shift_range=0.2,
    # height_shift_range=0.2,
)
train_data_generator = data_generator.flow_from_directory(
    "/home/falcon/Documents/machine_learning/data/dogs_gone_sideways/train",
    target_size=(image_size, image_size),
    color_mode="rgb",
    batch_size=10,
    class_mode="categorical",
)

validation_data_generator = data_generator.flow_from_directory(
    "/home/falcon/Documents/machine_learning/data/dogs_gone_sideways/val",
    target_size=(image_size, image_size),
    class_mode="categorical",
)

model.fit_generator(
    train_data_generator,
    steps_per_epoch=3,
    # epochs=2,
    validation_data=validation_data_generator,
    validation_steps=1,



x, y = train_data_generator.next()
for i in range(len(x)):
    image = x[i]
    plt.imshow(image)
    if y[i][0] == 1:
        # plt.title("sideways")
        print("sideways")
    else:
        print("correct")
    # plt.show()
print(model.predict(train_data_generator.next()))
