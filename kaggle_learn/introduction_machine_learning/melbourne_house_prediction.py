import pandas as pd
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_absolute_error
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeRegressor

data_path = r'~/Documents/Machine_Learning/data/melb_data.csv'
melbourne_data = pd.read_csv(data_path)

# print(melbourne_data.columns)
# print(melbourne_data.describe())

# drop null values
melbourne_data = melbourne_data.dropna(axis=0)

# choose features
melbourne_features = [
    'Rooms', 'Bathroom', 'Landsize', 'Lattitude', 'Longtitude'
]

# Hypothesis data
X = melbourne_data[melbourne_features]

# output
y = melbourne_data.Price

# Model
melbourne_model = DecisionTreeRegressor(random_state=1)
melbourne_model.fit(X, y)

# print(mean_absolute_error(y, melbourne_model.predict(X)))

# using train and validation set
train_X, val_X, train_y, val_y = train_test_split(X, y, random_state=0)
melbourne_model = DecisionTreeRegressor(random_state=1)
melbourne_model.fit(train_X, train_y)

val_predictions = melbourne_model.predict(val_X)
# print(mean_absolute_error(val_predictions, val_y))


def get_mae(max_leaf_nodes, train_X, val_X, train_y, val_y):
    model = DecisionTreeRegressor(max_leaf_nodes=max_leaf_nodes,
                                  random_state=0)
    model.fit(train_X, train_y)
    return mean_absolute_error(val_y, model.predict(val_X))


def key_of_min_value(dictionary):
    for k, v in dictionary.items():
        if v == min(dictionary.values()):
            return k


# print(
#     key_of_min_value({
#         i: get_mae(i, train_X, val_X, train_y, val_y)
#         for i in [5, 50, 500, 5000]
#     }))

# random tree forests regressor
melbourne_model = RandomForestRegressor(random_state=1)
melbourne_model.fit(X, y)
print(mean_absolute_error(melbourne_model.predict(X), y))
