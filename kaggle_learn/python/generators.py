# # generator are a simple way of creating iterator without the overhead of
# iterators
# execution stops after every iteration and control is transferred to caller
# this repeats till the function is terminated
# You can only go through a generator only once.

# def even_numbers(n):
#     # prints out 'n' even numbers starting from 2
#     num = 2
#     for _ in range(n):
#         result = num
#         num += 2
#         yield result

# a = even_numbers(10)
# i = iter(a)
# for _ in range(10):
#     print(a.__next__())

# for i in even_numbers(10):
#     print(i)

# generator expressions
# _list = [1, 2, 3, 4]

# _generator = (i ** 2 for i in _list)

# for i in _generator:
#     print(i)

# print(sum(i ** 2 for i in _list))

# power of 2 using generators
# def power_two(maximum = 0):
#     num = 0
#     while num < maximum:
#         yield 2 ** num
#         num += 1

# for i in power_two(10):
#     print(i)
