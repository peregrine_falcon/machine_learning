def conditional_probs(history):
    count = {}
    for i in range(1, len(history)):
        roll, prev = history[i], history[i - 1]

        if prev not in count:
            count[prev] = {}

        if roll not in count[prev]:
            count[prev][roll] = 0

        count[prev][roll] += 1

    # return count
    probs = {}
    for prev, nexts in count.items():
        total = sum(nexts.values())
        sub_probs = {num_count / total: next
                     for next, num_count in nexts.items()}
        probs[prev] = sub_probs

    return probs

probs_dict = conditional_probs([1, 2, 1, 3, 7])
print(probs_dict)

next_guess = probs_dict[2][max(probs_dict[2].keys())]

print(next_guess)
