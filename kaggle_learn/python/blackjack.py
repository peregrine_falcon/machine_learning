def blackjack_hand_greater_than(hand_1, hand_2):
    total_1, total_2 = hand_total(hand_1), hand_total(hand_2)
    return (total_1 <= 21) and (total_1 > total_2 or total_2 > 21)


def hand_total(hand):
    value_dict = {str(i): i for i in range(2, 11)}
    value_dict.update({'A': 11, 'J': 10, 'Q': 10, 'K': 10})
    values = [value_dict[i] for i in hand]

    count = sum(values)
    a_counts = values.count(11)

    while count > 21 and a_counts:
        count -= 10
        a_counts -= 1

    return count


print(blackjack_hand_greater_than(['2', '10', '5', 'A', '9', '9'],
                                  ['5', '7', '5', 'Q', '5']))
