# following example illustrate concept of closures

# Conditions to use closures
# 1. There should be use of a nested function, an outer function which houses
# an inner function
# 2. The outer function should have an attribute, which is a non local for the
# inner function
# 3. The outer function must return the inner function
# 4. The nested function object can be assigned to a variable.


def make_multiplier(n):

    def multiplier(x):
        return x * n
    return multiplier

mul_3 = make_multiplier(3)
mul_5 = make_multiplier(5)

print(mul_3(4), mul_5(4))
