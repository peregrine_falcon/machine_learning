#%% Import Data
import pandas as pd
data_path = "../../data/housing_prices_competition/melb_housing_prices.csv"
data = pd.read_csv(data_path)
data.head()

#%% [markdown]
# Handling Null Data: Analysis

#%% Checking for numerical null columns
print(data.select_dtypes(exclude="object").isnull().sum())

#%% Checking for object null columns
print(data.select_dtypes("object").isnull().sum())

#%% Numerical Column: Car
print(data.Car.value_counts())

# Use imputer with strategy as most_frequent
# as they are only 62 null values, moving them to most_frequent will not make
# much of a difference

#%% Numerical Column: BuildingArea
print(data.BuildingArea.value_counts())

# Drop column, as there are lot of values missing and as per my observation no
# effective method to handle the values

#%% Numerical Column: YearBuilt
print(data.YearBuilt.value_counts())

# Also drop the column, same problem as above, might be an important predictor
# but a lot of values are missig and I have no understanding of proper
# imputations method

#%% Categorical Column: CouncilArea
print(data.CouncilArea.value_counts())

# Move all the null values to Unavailable
# Above seems to be the better strategy as
# 1. Only about 10% of the data is null.
# 2. There seems to be a label Unavailble which is pretty obvious for null

#%% [markdown]
# Handling Null values: Execution

# Dropping columns with a lot of null values
columns_to_drop = ['BuildingArea', 'YearBuilt']
data.drop(columns_to_drop, axis=1, inplace=True)

#%% Impute Car column
car_freq = data.Car.mode().iloc[0]
data.Car.fillna(car_freq, inplace=True)

#%% Impute CouncilArea column
data.CouncilArea.fillna("Unavailable", inplace=True)

#%% [markdown]
# Handling Categorical Values: Analysis

#%%
categorical_columns = [
    col for col in data.columns
    if data[col].nunique() <= 35 and data[col].dtype == 'object'
]

data[categorical_columns].nunique().sum()

#%%
from sklearn.preprocessing import OneHotEncoder
one_hot_encoder = OneHotEncoder(sparse=False)
oh_encoded_data = pd.DataFrame(
    one_hot_encoder.fit_transform(data[categorical_columns]))

# adding back index
oh_encoded_data.index = data.index

#%%
# drop categorical columns
data.drop(categorical_columns, axis=1, inplace=True)

#%%
# drop remaining object columns
object_columns = [col for col in data.columns if data[col].dtype == 'object']

object_columns = list(set(object_columns) - set(categorical_columns))
data.drop(object_columns, axis=1, inplace=True)
# print(object_columns)

#%% final data set for training
data_final = pd.concat([data, oh_encoded_data], axis=1)
data_final.head()

#%% Model fitting
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_absolute_error, accuracy_score
from xgboost import XGBRegressor

# target
y = data_final.Price
X = data_final.drop(['Price'], axis=1)

X_train, X_valid, y_train, y_valid = train_test_split(X,
                                                      y,
                                                      train_size=0.8,
                                                      test_size=0.2,
                                                      random_state=42)

model = XGBRegressor(n_estimators=1000, learning_rate=0.1)
model.fit(X_train,
          y_train,
          eval_set=[(X_valid, y_valid)],
          early_stopping_rounds=10,
          verbose=False)

pred = model.predict(X_valid)

#%%
print(mean_absolute_error(pred, y_valid))

#%%
from sklearn.ensemble import RandomForestRegressor
model_dt = RandomForestRegressor(n_estimators=100, random_state=42)
model_dt.fit(X_train, y_train)

pred_dt = model_dt.predict(X_valid)
print(mean_absolute_error(pred_dt, y_valid))
