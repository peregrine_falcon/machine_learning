import pandas as pd
from sklearn.compose import ColumnTransformer
from sklearn.ensemble import RandomForestRegressor
from sklearn.impute import SimpleImputer
from sklearn.metrics import mean_absolute_error
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import OneHotEncoder

data_path = r"../../data/housing_prices_competition/melb_housing_prices.csv"
data = pd.read_csv(data_path)

# print(data.dtypes)

# Separate target from predictors
y = data.Price
X = data.drop(["Price"], axis=1)

# Split for training set and cross validation set
X_full_train, X_full_valid, y_train, y_valid = train_test_split(
    X, y, train_size=0.8, test_size=0.2, random_state=0
)

# Categorical columns having low cardinality
low_cardinal_cols = [
    col
    for col in X_full_train.columns
    if X_full_train[col].nunique() < 10 and X_full_train[col].dtype == "object"
]

numerical_cols = [
    col
    for col in X_full_train.columns
    if X_full_train[col].dtype in ["int64", "float64"]
]

model_cols = low_cardinal_cols + numerical_cols

X_train = X_full_train[model_cols].copy()
X_valid = X_full_valid[model_cols].copy()

# print(X_train.head())

# Pipe-lining

# pre-processing for numerical columns
numerical_transform = SimpleImputer(strategy="constant")

# pre-processing for categorical columns
categorical_transform = Pipeline(
    steps=[
        ("imputer", SimpleImputer(strategy="most_frequent")),
        ("onehot", OneHotEncoder(handle_unknown="ignore")),
    ]
)

preprocessor = ColumnTransformer(
    transformers=[
        ("numerical", numerical_transform, numerical_cols),
        ("categorical", categorical_transform, low_cardinal_cols),
    ]
)

model = RandomForestRegressor(n_estimators=100, random_state=0)

my_pipeline = Pipeline(steps=[("pre-process", preprocessor), ("model", model)])

my_pipeline.fit(X_train, y_train)
pred = my_pipeline.predict(X_valid)

print("MAE: ", mean_absolute_error(y_valid, pred))
