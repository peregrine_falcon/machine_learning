import pandas as pd
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_absolute_error
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import OneHotEncoder
from xgboost import XGBRegressor

data_path = "../../data/housing_prices_competition/train.csv"
data = pd.read_csv(data_path)

print("Shape Train: ", data.shape)
print(data.dtypes.value_counts())

#%%
data.select_dtypes(exclude="object").isnull().sum()

# columns
numerical_columns = [
    col for col in data.columns if data[col].dtype in ["int64", "float64"]
]
categorical_columns = [
    col
    for col in data.columns
    if data[col].dtype == "object" and data[col].nunique() <= 20
]

numerical_null_columns = [col for col in numerical_columns if data[col].isnull().any()]
categorical_null_columns = [
    col for col in categorical_columns if data[col].isnull().any()
]

#%%
# Strategy for numerical null columns

# print(data[numerical_null_columns].LotFrontage.plot.line()) # mean
# print(data[numerical_null_columns].MasVnrArea.plot.line())  # most_frequent
# fill values with corresponding YearBuilt
# print(data[numerical_null_columns].GarageYrBlt.plot.line())

# Operations for handling numerical null columns
data["GarageYrBlt"].fillna(data["YearBuilt"], inplace=True)
data["MasVnrArea"].fillna(data["MasVnrArea"].mode().iloc[0], inplace=True)
data["LotFrontage"].fillna(data["LotFrontage"].mean(), inplace=True)

#%%
# Stategy for categorical null values
# print(data[categorical_null_columns].isnull().sum())

# Drop Alley, PoolQC, MiscFeature, Fence
# print(data.MasVnrType.head()) # Replace with null
# print(data.FireplaceQu.head()) # Check cardinality; mostly dropping it
# rest replacing with most frequent
drop_list = ["Alley", "PoolQC", "Fence", "MiscFeature"]
data.drop(drop_list, axis=1, inplace=True)
remaining_null = list(set(categorical_null_columns) - set(drop_list))

for col in remaining_null:
    data[col].fillna(data[col].mode().iloc[0], inplace=True)

categorical_columns = list(set(categorical_columns) - set(drop_list))


oh_encoder = OneHotEncoder(handle_unknown="ignore", sparse=False)
oh_data = pd.DataFrame(oh_encoder.fit_transform(data[categorical_columns]))
oh_data.index = data[categorical_columns].index

#%%
# Model data
model_columns = categorical_columns + numerical_columns
model_data = data[model_columns].copy()
model_data.drop(categorical_columns, axis=1, inplace=True)
model_data = pd.concat([model_data, oh_data], axis=1)

#%%
model_data.isnull().sum().sum()


y = model_data.SalePrice
X = model_data.drop(["SalePrice"], axis=1)

X_train, X_valid, y_train, y_valid = train_test_split(
    X, y, train_size=0.8, test_size=0.2, random_state=42
)

# XGB Regressor
model = XGBRegressor(n_estimators=1000, learning_rate=0.5)
model.fit(
    X_train,
    y_train,
    eval_set=[(X_valid, y_valid)],
    early_stopping_rounds=10,
    verbose=False,
)

# Random forest regressor
pred = model.predict(X_valid)
print("XGBRegressor: ", mean_absolute_error(pred, y_valid))

model = RandomForestRegressor(n_estimators=1000, criterion="mse", random_state=42)
model.fit(X_train, y_train)

pred = model.predict(X_valid)
print("RandomForestRegressor: ", mean_absolute_error(pred, y_valid))
