import pandas as pd
from sklearn.compose import ColumnTransformer
from sklearn.ensemble import RandomForestRegressor
from sklearn.impute import SimpleImputer
from sklearn.metrics import mean_absolute_error
from sklearn.model_selection import cross_val_score, train_test_split
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import OneHotEncoder
from xgboost import XGBRegressor

data_path = "../../data/housing_prices_competition/melb_housing_prices.csv"
data = pd.read_csv(data_path)

# print("Describe\n", data.describe())
# print("Head\n", data.head(10))
# print("Shape\n", data.shape)
# print("Dtypes\n", data.dtypes)

# model evaluation function
def random_forest_regressor_model(X_t, X_v, y_t, y_v):
    model = RandomForestRegressor(n_estimators=100, criterion="mse", random_state=42)
    model.fit(X_t, y_t)
    return mean_absolute_error(model.predict(X_v), y_v)


# Approach 1: Dropping null columns
def Approach_1(data):
    # feature selection
    num_cols = [col for col in data.columns if data[col].dtype in ["float64", "int64"]]

    y = data.Price  # target
    X = data[num_cols].drop(["Price"], axis=1)  # predictors

    # data pre-processing

    # handle null and categorical values
    null_cols = [col for col in X.columns if X[col].isnull().any()]
    X.drop(null_cols, axis=1, inplace=True)

    # there is no possibility of categorical data as only numerical data is used

    # splitting training and training set randomly
    X_train, X_valid, y_train, y_valid = train_test_split(
        X, y, train_size=0.8, test_size=0.2, random_state=42
    )

    print(
        "Approach 1: ",
        random_forest_regressor_model(X_train, X_valid, y_train, y_valid),
    )


# Approach 2: Imputation with extension
def Approach_2(data):
    # feature selection
    num_cols = [col for col in data.columns if data[col].dtype in ["float64", "int64"]]

    y = data.Price  # target
    X = data[num_cols].drop(["Price"], axis=1)  # predictors

    # data pre-processing

    # handling null values
    null_cols = [col for col in X.columns if X[col].isnull().any()]

    # imputation with extension
    for col in null_cols:
        X[col + "_missing"] = X[col].isnull()

    # split train and test
    X_train, X_valid, y_train, y_valid = train_test_split(
        X, y, train_size=0.8, test_size=0.2, random_state=42
    )

    imputer = SimpleImputer()
    imputer_X_train = pd.DataFrame(imputer.fit_transform(X_train))
    imputer_X_valid = pd.DataFrame(imputer.transform(X_valid))

    # add columns back as imputation removes columns
    imputer_X_train.columns = X_train.columns
    imputer_X_valid.columns = X_valid.columns

    print(
        "Approach 2: ",
        random_forest_regressor_model(
            imputer_X_train, imputer_X_valid, y_train, y_valid
        ),
    )


# Approch 3: Both Imputation and One hot encoding
def Approach_3(data):

    y_full = data.Price  # target
    X_full = data.drop(["Price"], axis=1)  # predictors

    numerical_columns = [
        col for col in X_full.columns if X_full[col].dtype in ["float64", "int64"]
    ]

    categorical_columns = [
        col
        for col in X_full
        if X_full[col].nunique() <= 15 and X_full[col].dtype == "object"
    ]

    cols = numerical_columns + categorical_columns
    X = X_full[cols]

    X_train, X_valid, y_train, y_valid = train_test_split(
        X, y_full, train_size=0.8, test_size=0.2, random_state=42
    )

    # Imputation with extension on numerical columns
    # TODO handling date columns separately
    imputer = SimpleImputer()
    num_X_train = X_train[numerical_columns].copy()
    num_X_valid = X_valid[numerical_columns].copy()

    null_cols = [col for col in num_X_train.columns if num_X_train[col].isnull().any()]

    for col in null_cols:
        num_X_train[col + "_missing"] = num_X_train[col].isnull()
        num_X_valid[col + "_missing"] = num_X_valid[col].isnull()

    imputer_X_train = pd.DataFrame(imputer.fit_transform(num_X_train))
    imputer_X_valid = pd.DataFrame(imputer.transform(num_X_valid))

    imputer_X_train.columns = num_X_train.columns
    imputer_X_valid.columns = num_X_valid.columns

    # One hot encoding on categorical columns
    # TODO handling null columns
    one_hot_encoder = OneHotEncoder(handle_unknown="ignore", sparse=False)
    cat_X_train = X_train[categorical_columns].copy()
    cat_X_valid = X_valid[categorical_columns].copy()

    oh_train_cols = pd.DataFrame(one_hot_encoder.fit_transform(cat_X_train))
    oh_valid_cols = pd.DataFrame(one_hot_encoder.transform(cat_X_valid))

    oh_train_cols.index = X_train.index
    oh_valid_cols.index = X_valid.index

    # reset indexes before concatenation
    imputer_X_train.reset_index(drop=True, inplace=True)
    imputer_X_valid.reset_index(drop=True, inplace=True)
    oh_train_cols.reset_index(drop=True, inplace=True)
    oh_valid_cols.reset_index(drop=True, inplace=True)

    # concatenation
    X_train_concat = pd.concat([imputer_X_train, oh_train_cols], axis=1)
    X_valid_concat = pd.concat([imputer_X_valid, oh_valid_cols], axis=1)

    print(
        "Approach 3: ",
        random_forest_regressor_model(X_train_concat, X_valid_concat, y_train, y_valid),
    )


# Approach 4: Using pipelines
def Approach_4(data):
    y_full = data.Price  # target
    X_full = data.drop(["Price"], axis=1)  # predictors

    X_train_full, X_valid_full, y_train, y_valid = train_test_split(
        X_full, y_full, train_size=0.8, test_size=0.2, random_state=42
    )

    # numerical columns
    numerical_columns = [
        col
        for col in X_train_full.columns
        if X_train_full[col].dtype in ["float64", "int64"]
    ]

    # categorical columns
    categorical_columns = [
        col
        for col in X_train_full.columns
        if X_train_full[col].dtype == "object" and X_train_full[col].nunique() <= 15
    ]

    # preprocessing
    numerical_transform = SimpleImputer(strategy="constant")
    categorical_transform = Pipeline(
        steps=[
            ("imputer", SimpleImputer(strategy="most_frequent")),
            ("one hot", OneHotEncoder(handle_unknown="ignore", sparse=False)),
        ]
    )

    preprocessor = ColumnTransformer(
        transformers=[
            ("numerical_transform", numerical_transform, numerical_columns),
            ("categorical_transform", categorical_transform, categorical_columns),
        ]
    )

    # model fitting
    model = RandomForestRegressor(n_estimators=100, random_state=42)
    training_pipeline = Pipeline(
        steps=[("pre-processing", preprocessor), ("model fitting", model)]
    )

    training_pipeline.fit(X_train_full, y_train)

    predictions = training_pipeline.predict(X_valid_full)

    print("Approach 4: ", mean_absolute_error(predictions, y_valid))


# Approach 5: Cross validation
def Approach_5(data):
    y_full = data.Price  # target
    X_full = data.drop(["Price"], axis=1)  # predictors

    # numerical columns
    numerical_columns = [
        col for col in X_full.columns if X_full[col].dtype in ["float64", "int64"]
    ]

    # categorical columns
    categorical_columns = [
        col
        for col in X_full.columns
        if X_full[col].dtype == "object" and X_full[col].nunique() <= 15
    ]

    # preprocessing
    numerical_transform = SimpleImputer(strategy="constant")
    categorical_transform = Pipeline(
        steps=[
            ("imputer", SimpleImputer(strategy="most_frequent")),
            ("one hot", OneHotEncoder(handle_unknown="ignore", sparse=False)),
        ]
    )

    preprocessor = ColumnTransformer(
        transformers=[
            ("numerical_transform", numerical_transform, numerical_columns),
            ("categorical_transform", categorical_transform, categorical_columns),
        ]
    )

    # model fitting
    model = RandomForestRegressor(n_estimators=100, random_state=42)
    training_pipeline = Pipeline(
        steps=[("pre-processing", preprocessor), ("model fitting", model)]
    )

    scores = -1 * cross_val_score(
        training_pipeline, X_full, y_full, cv=5, scoring="neg_mean_absolute_error"
    )
    print("Approach 4: ", scores)


# Approach 6: XGBoostRegressor without pipeline
def Approach_6(data):

    y_full = data.Price  # target
    X_full = data.drop(["Price"], axis=1)  # predictors

    numerical_columns = [
        col for col in X_full.columns if X_full[col].dtype in ["float64", "int64"]
    ]

    categorical_columns = [
        col
        for col in X_full
        if X_full[col].nunique() <= 15 and X_full[col].dtype == "object"
    ]

    cols = numerical_columns + categorical_columns
    X = X_full[cols]

    X_train, X_valid, y_train, y_valid = train_test_split(
        X, y_full, train_size=0.8, test_size=0.2, random_state=42
    )

    # Imputation with extension on numerical columns
    # TODO handling date columns separately
    imputer = SimpleImputer()
    num_X_train = X_train[numerical_columns].copy()
    num_X_valid = X_valid[numerical_columns].copy()

    null_cols = [col for col in num_X_train.columns if num_X_train[col].isnull().any()]

    for col in null_cols:
        num_X_train[col + "_missing"] = num_X_train[col].isnull()
        num_X_valid[col + "_missing"] = num_X_valid[col].isnull()

    imputer_X_train = pd.DataFrame(imputer.fit_transform(num_X_train))
    imputer_X_valid = pd.DataFrame(imputer.transform(num_X_valid))

    imputer_X_train.columns = num_X_train.columns
    imputer_X_valid.columns = num_X_valid.columns

    # One hot encoding on categorical columns
    # TODO handling null categorical columns
    one_hot_encoder = OneHotEncoder(handle_unknown="ignore", sparse=False)
    cat_X_train = X_train[categorical_columns].copy()
    cat_X_valid = X_valid[categorical_columns].copy()

    oh_train_cols = pd.DataFrame(one_hot_encoder.fit_transform(cat_X_train))
    oh_valid_cols = pd.DataFrame(one_hot_encoder.transform(cat_X_valid))

    oh_train_cols.index = X_train.index
    oh_valid_cols.index = X_valid.index

    # reset indexes before concatenation
    imputer_X_train.reset_index(drop=True, inplace=True)
    imputer_X_valid.reset_index(drop=True, inplace=True)
    oh_train_cols.reset_index(drop=True, inplace=True)
    oh_valid_cols.reset_index(drop=True, inplace=True)

    # concatenation
    X_train_concat = pd.concat([imputer_X_train, oh_train_cols], axis=1)
    X_valid_concat = pd.concat([imputer_X_valid, oh_valid_cols], axis=1)

    model = XGBRegressor(n_estimators=1000, learning_rate=0.05)
    model.fit(
        X_train_concat,
        y_train,
        eval_set=[(X_valid_concat, y_valid)],
        early_stopping_rounds=10,
        verbose=False,
    )
    print("Approach 6:", mean_absolute_error(model.predict(X_valid_concat), y_valid))


# Approach 7: XGBoost regression
# def Approach_7(data):
#     y_full = data.Price  # target
#     X_full = data.drop(["Price"], axis=1)  # predictors

#     X_train_full, X_valid_full, y_train, y_valid = train_test_split(
#         X_full, y_full, train_size=0.8, test_size=0.2, random_state=42
#     )

#     # numerical columns
#     numerical_columns = [
#         col
#         for col in X_train_full.columns
#         if X_train_full[col].dtype in ["float64", "int64"]
#     ]

#     # categorical columns
#     categorical_columns = [
#         col
#         for col in X_train_full.columns
#         if X_train_full[col].dtype == "object" and X_train_full[col].nunique() <= 15
#     ]

#     # preprocessing
#     numerical_transform = SimpleImputer(strategy="constant")
#     categorical_transform = Pipeline(
#         steps=[
#             ("imputer", SimpleImputer(strategy="most_frequent")),
#             ("one hot", OneHotEncoder(handle_unknown="ignore", sparse=False)),
#         ]
#     )

#     preprocessor = ColumnTransformer(
#         transformers=[
#             ("numerical_transform", numerical_transform, numerical_columns),
#             ("categorical_transform", categorical_transform, categorical_columns),
#         ]
#     )

#     # model fitting
#     model = XGBRegressor(n_estimators=1000, learning_rate=0.05)
#     training_pipeline = Pipeline(
#         steps=[("pre-processing", preprocessor), ("model", model)]
#     )

#     training_pipeline.fit(
#         X_train_full,
#         y_train,
#         model__eval_set=[(X_valid_full, y_valid)],
#         model__early_stopping_rounds=10,
#         model__verbose=False,
#     )

#     predictions = training_pipeline.predict(X_valid_full)

#     print("Approach 7: ", mean_absolute_error(predictions, y_valid))


Approach_1(data)
Approach_2(data)
Approach_3(data)
Approach_4(data)
Approach_5(data)
Approach_6(data)
Approach_7(data)
