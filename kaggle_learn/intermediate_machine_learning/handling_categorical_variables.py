import pandas as pd
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_absolute_error
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder, OneHotEncoder

data_path = '../../data/housing_prices_competition/melb_housing_prices.csv'
data = pd.read_csv(data_path)

# print(data.dtypes)

y = data.Price
X = data.drop(['Price'], axis=1)

X_train_full, X_valid_full, y_train, y_valid = train_test_split(X,
                                                                y,
                                                                train_size=0.8,
                                                                test_size=0.2,
                                                                random_state=0)

# columns with null values
null_columns = [
    col for col in X_train_full.columns if X_train_full[col].isnull().any()
]
# print(null_columns)

# dropping null columns for the sake of simplicity
X_train_full.drop(null_columns, axis=1, inplace=True)
X_valid_full.drop(null_columns, axis=1, inplace=True)

# print(X_train_full.dtypes)

# Cardinality
# Number of unique values in a column, selecting values with low-cardinality

low_cardinal_cols = [
    col for col in X_train_full.columns
    if X_train_full[col].nunique() < 10 and X_train_full[col].dtype == "object"
]

# numerical columns
numerical_cols = [
    col for col in X_train_full.columns
    if X_train_full[col].dtype in ["int64", "float64"]
]

model_columns = low_cardinal_cols + numerical_cols

X_train = X_train_full[model_columns].copy()
X_valid = X_valid_full[model_columns].copy()


# function to measure the quality of each approach
def score_dataset(X_t, X_v, y_t=y_train, y_v=y_valid):
    model = RandomForestRegressor(n_estimators=100)
    model.fit(X_t, y_t)
    return mean_absolute_error(y_v, model.predict(X_v))


# Approach 1: Dropping categorical values
drop_X_train = X_train.select_dtypes(exclude=['object'])
drop_X_valid = X_valid.select_dtypes(exclude=['object'])
print("Approach 1: Dropping categorical values, MAE: ",
      score_dataset(drop_X_train, drop_X_valid))

# Approach 2: Apply Label encoder to each column with categorical data
label_X_train = X_train.copy()
label_X_valid = X_valid.copy()

label_encoder = LabelEncoder()
for col in low_cardinal_cols:
    label_X_train[col] = label_encoder.fit_transform(X_train[col])
    label_X_valid[col] = label_encoder.transform(X_valid[col])

print("Approach 2: Applying label encoder, MAE: ",
      score_dataset(label_X_train, label_X_valid))

# Approach 3: One hot encoder
one_hot_encoder = OneHotEncoder(handle_unknown='ignore', sparse=False)
oh_train_colums = pd.DataFrame(
    one_hot_encoder.fit_transform(X_train[low_cardinal_cols]))

oh_valid_colums = pd.DataFrame(
    one_hot_encoder.transform(X_valid[low_cardinal_cols]))

# one hot encoding removed index add index columns
oh_train_colums.index = X_train.index
oh_valid_colums.index = X_valid.index

# dropping object columns, to be replaced with one hot columns
num_X_train = X_train.drop(low_cardinal_cols, axis=1)
num_X_valid = X_valid.drop(low_cardinal_cols, axis=1)

# add one hot columns
OH_X_train = pd.concat([num_X_train, oh_train_colums], axis=1)
OH_X_valid = pd.concat([num_X_valid, oh_valid_colums], axis=1)

print("Approach 3: Applying label encoder, MAE: ",
      score_dataset(OH_X_train, OH_X_valid))
