#%%
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

sns.set_style("whitegrid")

train_data_path = "../../data/housing_prices_competition/train.csv"
train = pd.read_csv(train_data_path)

correlation = train.corr()
f, ax = plt.subplots(figsize=(14, 12))
plt.title('Correlation of numerical attributes', size=16)
_ = sns.heatmap(correlation)


#%%
train.select_dtypes('object').nunique().sort_values(ascending=False)

#%%
train.select_dtypes('object').describe().loc['freq'].sort_values(ascending=False)/len(train)