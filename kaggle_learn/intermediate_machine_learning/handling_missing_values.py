import pandas as pd
from sklearn.ensemble import RandomForestRegressor
from sklearn.impute import SimpleImputer
from sklearn.metrics import mean_absolute_error
from sklearn.model_selection import train_test_split

data_path = r'../../data/housing_prices_competition/melb_housing_prices.csv'
data = pd.read_csv(data_path)

# output and hypothesis
y_full = data.Price
X_full = data.drop(['Price'], axis=1).select_dtypes(exclude=['object'])

# print(X_full.dtypes)

X_train, X_valid, y_train, y_valid = train_test_split(X_full,
                                                      y_full,
                                                      train_size=0.8,
                                                      test_size=0.2,
                                                      random_state=0)


# Function to test the performance of various techniques to handle null values
def score_dataset(X_t, X_v, y_t=y_train, y_v=y_valid):
    model = RandomForestRegressor(n_estimators=10)
    model.fit(X_t, y_t)
    return mean_absolute_error(y_v, model.predict(X_v))


# Approach 1: Dropping columns with null data
null_columns = [col for col in X_train.columns if X_train[col].isnull().any()]
reduced_X_train = X_train.drop(null_columns, axis=1)
reduced_X_valid = X_valid.drop(null_columns, axis=1)

print("MAE(Dropping Columns)", score_dataset(reduced_X_train, reduced_X_valid))

# Approach 2: Imputation, filling of null values with the mean of the
# neighbouring values

my_imputer = SimpleImputer()
imputed_X_train = pd.DataFrame(my_imputer.fit_transform(X_train))
imputed_X_valid = pd.DataFrame(my_imputer.transform(X_valid))

# imputation removes column names, adding them back
imputed_X_train.columns = X_train.columns
imputed_X_valid.columns = X_valid.columns

print("MAE(Imputation)", score_dataset(imputed_X_train, imputed_X_valid))

# Approach 3: Imputation with extension of a is null column

# copy data to avoid changing the original data
X_train_plus = X_train.copy()
X_valid_plus = X_valid.copy()

# add a new extension column to each feature column having null values
for col in null_columns:
    X_train_plus[col + "_missing"] = X_train_plus[col].isnull()
    X_valid_plus[col + "_missing"] = X_valid_plus[col].isnull()

# imputation
imputer = SimpleImputer()
imputed_X_train = pd.DataFrame(imputer.fit_transform(X_train_plus))
imputed_X_valid = pd.DataFrame(imputer.transform(X_valid_plus))

# adding columns back as imputation removes columns names
imputed_X_train.columns = X_train_plus.columns
imputed_X_valid.columns = X_valid_plus.columns

print("MAE(Imputation with extension)",
      score_dataset(imputed_X_train, imputed_X_valid))

# columns with null values
print(X_train.shape)
missing_values_count_by_column = X_train.isnull().sum()
print(missing_values_count_by_column[missing_values_count_by_column > 0])
