import pandas as pd
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_absolute_error
from sklearn.model_selection import train_test_split

train_path = r'../../data/housing_prices_competition/train.csv'
test_path = r'../../data/housing_prices_competition/test.csv'

# train and test data
X_train_full = pd.read_csv(train_path, index_col='Id')
X_test_full = pd.read_csv(test_path, index_col='Id')

# target and predictors
# print(X_train_full.columns)
y = X_train_full.SalePrice
features = [
    'LotArea', 'YearBuilt', '1stFlrSF', '2ndFlrSF', 'FullBath', 'BedroomAbvGr',
    'TotRmsAbvGrd'
]
X = X_train_full[features].copy()
X_test = X_test_full[features].copy()

# split data for training and cross-validation
X_train, X_valid, y_train, y_valid = train_test_split(X,
                                                      y,
                                                      train_size=0.8,
                                                      test_size=0.2,
                                                      random_state=0)

# print(X_train.head())

# define models
model_1 = RandomForestRegressor(n_estimators=50, random_state=0)
model_2 = RandomForestRegressor(n_estimators=100, random_state=0)
model_3 = RandomForestRegressor(n_estimators=100,
                                criterion="mae",
                                random_state=0)
model_4 = RandomForestRegressor(n_estimators=200,
                                min_samples_split=20,
                                random_state=0)
model_5 = RandomForestRegressor(n_estimators=100, max_depth=7, random_state=0)

models = [model_1, model_2, model_3, model_4, model_5]


# fitting models
def score_models(model, X_t=X_train, X_v=X_valid, y_t=y_train, y_v=y_valid):
    model.fit(X_t, y_t)
    return mean_absolute_error(y_v, model.predict(X_v))


# rating models based on performance
model_count = 1
for model in models:
    print(model_count, score_models(model))
    model_count += 1
