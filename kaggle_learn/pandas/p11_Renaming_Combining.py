import pandas as pd

pd.set_option('display.max_rows', 15)

data_path = r'~/Documents/Code/kaggle_python/data/winemag-data-130k-v2.csv'
# data_path = r'P:\Programming\kaggle_python\data\winemag-data-130k-v2.csv'
reviews = pd.read_csv(data_path, index_col=0)

# rename points column as score
renamed_df = reviews.rename(columns={'points': 'score'})
# print(renamed_df.dtypes)

# rename index
renamed_df = reviews.rename(index={0: 'first_row', 1: 'second_row'})
# print(renamed_df)

# rename axis variables
renamed_df = reviews.rename_axis("wines",
                                 axis='rows').rename_axis("fields",
                                                          axis='columns')
# print(renamed_df)

CA_data_path = '~/Documents/Code/kaggle_python/data/youtube_data/CAvideos.csv'
GB_data_path = '~/Documents/Code/kaggle_python/data/youtube_data/GBvideos.csv'

canadian_youtube = pd.read_csv(CA_data_path)
british_youtube = pd.read_csv(GB_data_path)

# combine both the data frames
# print(canadian_youtube.shape)
# print(british_youtube.shape)
combined = pd.concat([canadian_youtube, british_youtube])
# print(combined.shape)

# join the dataframes
left = canadian_youtube.set_index(['title', 'trending_date'])
right = british_youtube.set_index(['title', 'trending_date'])
# print(left.head())
# print(right.head())

joined = left.join(right, lsuffix='_CAN', rsuffix='_GB')
print(joined)
