import quandl
import pandas as pd

fp = r'D:\Code\git_repos\kaggle_python\data'
api_key = "hhsS_xRr_wnB-6nvHDUa"

df = quandl.get('FMAC/HPI_AK', authtoken=api_key)
# print(df.head())

# html_df is a list of data frames and not a single data frame
html_df = pd.read_html('https://simple.wikipedia.org/wiki/List_of_U.S._states')

# use html_df[0] to use the first dataframe
# use html_df[0][1] to use second column of the first data frame
state_list = html_df[0][1]
print(state_list)

quandl_id = []
for i in state_list[1:]:
    quandl_id.append('FMAC/HPI_' + str(i))
