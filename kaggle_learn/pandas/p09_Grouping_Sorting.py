import pandas as pd

pd.set_option('display.max_rows', 10)

data_path = '~/Documents/Code/kaggle_python/data/winemag-data-130k-v2.csv'
reviews = pd.read_csv(data_path)

groupby_points_count = reviews.groupby('points').points.count()
# print(groupby_points_count)

groupby_price_min = reviews.groupby('points').price.min()
# print(groupby_points_min)

groupby_title = reviews.groupby('winery').apply(lambda df: df.title.iloc[0])
# print(groupby_title)

groupby_province = reviews.groupby(
    ['country', 'province']).apply(lambda df: df.title.loc[df.points.idxmax()])
# print(groupby_province)

groupby_agg = reviews.groupby('country').price.agg([len, min, max, sum])
# print(groupby_agg)

# group aggregate with multiple indices
groupby_multiple = reviews.groupby(['country',
                                    'province']).description.agg(len)
groupby_multiple_reset_index = groupby_multiple.reset_index()
# print(groupby_multiple_reset_index)

# sorting
countries_reviewed = reviews.groupby(['country',
                                      'province']).description.agg(len)

# print(countries_reviewed.sort_values())
test = countries_reviewed.reset_index()
# print(test.sort_values(by=['description']))
# print(test.sort_values(by=['description'], ascending=False))
# print(test.sort_values(by=['country', 'description']))

# reviews written, grouped by twitter handle
# print(reviews.columns)
reviews_written = reviews.groupby(
    'taster_twitter_handle').taster_twitter_handle.count()
# print(reviews_written)

# best wine for the given amount of money
reviews_best_wine = reviews.groupby(['price', 'title']).points.max()
best_wine = reviews.loc[(reviews.price == 4.0) &
                        (reviews.points == 86), 'title']
# print(reviews_best_wine)
# print(best_wine)
# print(reviews_best_wine.reset_index().sort_values(by='price'))

# Min and Max price of each variety of wine
price_extremes = reviews.groupby('variety').price.agg([min, max])
sorted_varieties = price_extremes.sort_values(by=['min', 'max'],
                                              ascending=False)
# print(sorted_varieties)

# average of points given by each reviewer
reviewer_average_score = reviews.groupby('taster_name').points.mean()
# print(reviewer_average_score.describe())

# most common combination of country and wine
common_wine = reviews.groupby(['country', 'variety'
                               ]).variety.count().sort_values(ascending=False)
print(common_wine)
