import pandas as pd
fp = r'D:\Code\git_repos\kaggle_python\data'

df = pd.read_csv(fp + r'\housing_prices_zillow.csv')
# print(df.head())

# set_index returns a new data frame if inplace=True is not passed
df.set_index('Date', inplace=True)
# print(df.head())

df.to_csv(fp + 'output_sample.csv')
# index can be set during import
df2 = pd.read_csv(fp + 'output_sample.csv', index_col=0)
# print(df2.head())

# renaming all columns, In this case there is only one column
# since date is an index, Index is not a column
df.columns = ['Austin_HPI']
# print(df.head())

# output with no headers
# note header=False only works while exporting
# it does not work while importing
df.to_csv(fp + 'headerless.csv', header=False)

# column headers can be set while importing
df3 = pd.read_csv(fp + 'headerless.csv',
                  names=['Date', 'Austin_HPI'], index_col=0)
print(df3.head())

df4 = pd.read_csv(fp + 'headerless.csv',
                  names=['Date', 'Austin_HDI'])
# print(df4.head())

# renaming a single column
df4.rename(columns={'Austin_HDI': '77660_HDI'}, inplace=True)
# print(df4.head())

# export to html table
df.to_html(fp + 'example.html')
