import pandas as pd

pd.set_option('display.max_rows', 10)

data_path = '~/Documents/Code/kaggle_python/data/winemag-data-130k-v2.csv'
reviews = pd.read_csv(data_path)

# view data
# print(reviews.head())

# view description
# print(reviews.describe())

# list columns
# print(list(reviews.columns))

# select a single column named description
desc = reviews.description
# print(desc)

# select first 10 rows of the column via index
# note iloc follows python indexing and loc does note
# for loc use one less number than iloc for the same number of results

desc = reviews.description.iloc[:10]
# print(desc)

# select multiple columns via label: locater: loc
data = reviews.loc[:9, ['country', 'points', 'price']]
# print(data)

# select multiple columns via index: integer locater: iloc
indices = [
    reviews.columns.get_loc('country'),
    reviews.columns.get_loc('points'),
    reviews.columns.get_loc('price')
]
# print(reviews.iloc[:10, indices])

# select multiple rows
rows = [1, 2, 4, 6, 9]
# print(reviews.iloc[rows, indices])
# print(reviews.loc[rows, ['country', 'points', 'price']])

# reviews of wines made in Italy
italian_wine_reviews = reviews[reviews.country == 'Italy']
# print(italian_wine_reviews.iloc[:10, :])

# reviews of wines made in oceania with at least 95 points
oceania_wine_reviews = reviews[(
    reviews.points >= 95) & reviews.country.isin(['Australia', 'New Zealand'])]
print(oceania_wine_reviews.country.value_counts())
