import pandas as pd

pd.set_option('display.max_rows', 15)

# data_path = '~/Documents/Code/kaggle_python/data/winemag-data-130k-v2.csv'
data_path = r'P:\Programming\kaggle_python\data\winemag-data-130k-v2.csv'
reviews = pd.read_csv(data_path, index_col=0)

# to find the data types of each column
# print(reviews.price.dtype)
# print(reviews.dtypes)
# print(reviews.index.dtype)

# converting data type of one columns
# print(reviews.points.astype('float64'))

# data frame consisting of records having country as null
reviews_null_countries = reviews[reviews.country.isnull()]
# print(reviews_null_countries)

# print(reviews.region_2[reviews.region_2.isnull()])
filled = reviews.region_2.fillna("Unknown")
# print(filled.value_counts())
# print(reviews_null_countries.country.count())
# print(len(reviews_null_countries.country.index))

# replacing twitter handle
replaced = reviews.taster_twitter_handle.replace("@kerinokeefe", "@kerino")
# print(replaced)

# count of total null price values
null_count = reviews.price.isnull().sum()
# print(null_count)

# most common wine producing regions
regions = reviews.region_1.fillna("Unknown").value_counts()
print(regions)
