import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import style
import numpy as np
style.use('ggplot')

web_stats = {
    'Day': [1, 2, 3, 4, 5, 6],
    'Visitors': [12, 13, 13, 14, 15, 11],
    'Bounce_Rate': [5, 5, 6, 7, 4, 6]
}

df = pd.DataFrame(web_stats)
df.set_index('Day', inplace=True)

# print(df.Visitors)

# print(df[['Bounce_Rate', 'Visitors']])

# print(df.Visitors.tolist())

# gives error
# print(df[['Bounce_Rate', 'Visitors']].tolist())

# to print above use numpy which gives arry
# print(np.array(df[['Bounce_Rate', 'Visitors']]))

df2 = pd.DataFrame(np.array(df[['Bounce_Rate', 'Visitors']]))
print(df2.head)
