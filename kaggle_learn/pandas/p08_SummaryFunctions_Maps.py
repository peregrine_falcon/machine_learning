import pandas as pd

pd.set_option('display.max_rows', 10)

data_path = '~/Documents/Code/kaggle_python/data/winemag-data-130k-v2.csv'
reviews = pd.read_csv(data_path)

# median of points column in the data
median = reviews.points.median()
# print(median)

# number of unique countries represented in the data set
# print(list(reviews.columns))
countries = reviews.country.unique()
# print(countries)

# reviews per country
reviews_per_country = reviews.country.value_counts()
# print(reviews_per_country)

# centered price: actual price - mean price
centered_price = reviews.price - reviews.price.mean()
# print(centered_price)

# best wine according to points to price ratio
best_wine = reviews.loc[(reviews.points / reviews.price).idxmax, ['title']]
# print(best_wine)

# find the total usage of the words fruity and tropical in wine review
# descriptions
n_fruity = reviews.description.map(lambda desc: "fruity" in desc).sum()
n_tropical = reviews.description.map(lambda desc: "tropical" in desc).sum()
total_usage = pd.Series([n_fruity, n_tropical], index=['fruity', 'tropical'])

# print(total_usage)


# apply ratings based on conditions
def ratings(row):
    if row.country == 'Canada':
        return 3
    elif row.points >= 95:
        return 3
    elif row.points >= 85:
        return 2
    else:
        return 1


star_ratings = reviews.apply(ratings, axis='columns')
# print(star_ratings.value_counts())
