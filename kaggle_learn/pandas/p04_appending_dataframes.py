import pandas as pd

df1 = pd.DataFrame({'HPI': [80, 85, 88, 85],
                    'Int_rate': [2, 3, 2, 2],
                    'US_GDP_Thousands': [50, 55, 65, 55]},
                   index=[2001, 2002, 2003, 2004])

df2 = pd.DataFrame({'HPI': [80, 85, 88, 85],
                    'Int_rate': [2, 3, 2, 2],
                    'US_GDP_Thousands': [50, 55, 65, 55]},
                   index=[2005, 2006, 2007, 2008])

df3 = pd.DataFrame({'HPI': [80, 85, 88, 85],
                    'Int_rate': [2, 3, 2, 2],
                    'Low_tier_HPI': [50, 52, 50, 53]},
                   index=[2001, 2002, 2003, 2004])

# since df1 and df2 have different indexes concat
# just combines rows of df1 to df2
# NOTE order matters while concatenating
concat = pd.concat([df1, df2])
print(concat)
concat_1 = pd.concat([df1, df2, df3], sort=True)
# print(concat_1)

# appending one data frame to another
# adds the passed dataframe to the end
append = df1.append(df2)
# print(append)
# again append and concat is not very intuitive
append_1 = df1.append(df3, sort=False)
# print(append_1)
